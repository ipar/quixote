# Quixote

![Don Quixote by Pablo Picasso](docs/images/don_quixote.jpg)

The complete documentation can be found in: https://ipar.gitlab.io/quixote

Python 3.x tools to deal effectively with SOLPS5.0 and SOLPS-ITER data stored in the MDS+ server and/or run directories.

This package is shared with the aim to create a community based post-processing tool.
However, this is not an official SOLPS routine. The developers have tried to balance simplicity for the users with completness.
Do not use this routine with blind acceptance. Always keep an eye for posisble bugs and report them to the developer.


## Installation
See : https://ipar.gitlab.io/quixote/installation.html

## Testing
See : https://ipar.gitlab.io/quixote/guides/testing.html

## List of authors

 - Ivan Paradela Perez, original author and general developer
 - Ferdinand Hitzler, main author of chords and spectroscopy
