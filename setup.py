import setuptools

setuptools.setup(
    name='quixote',
    version='0.1.0',
    author='Ivan Paradela Perez',
    url='https://gitlab.com/ipar/quixote',
    description='Python 3 package to work with SOLPS simulations',
    packages = ['quixote'],
    python_requires='>=3.7',

    install_requires=[
        'numpy',
        'scipy',
        'matplotlib',
        'pandas',
        'tqdm',
        'PyYAML',
        'pytest',
        'pytest-cov',
        'pytest-randomly',
        'f90nml',
        ],

    extras_require={
        #For documentation
        'docs':[ 
            'sphinx',
            'ipykernel',
            'nbconvert>=7',
            'mistune>=2',
            'nbsphinx',
            'jupyter-sphinx',
            'sphinx_rtd_theme',
        ]
    }
)
