__version__ = '0.1.0'




import logging
slog = logging.getLogger('quixote')
if not slog.handlers: #Prevents from adding repeated streamhandlers
    formatter = logging.Formatter(
            '%(name)s -- %(levelname)s: %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    slog.addHandler(ch)
    slog.propagate = False #Otherwise, root logger also prints




## Classes
from .classes.factory import SolpsData
from .classes.family import Family
from .classes.family import Uncertainty

## Plots
from .classes.plots import VesselPlot, GridDataPlot, TriangDataPlot
#from .classes.plots import Error2DLine

## Tools
from .tools import module_path, read_config
from .tools import update, extend, get, whichclass
from .tools import errorplot
from .tools import cumsum_quantiles
from .tools import contours
from .tools import point_hull
from .tools import legend

__all__ = [
    'SolpsData',
    'Family',
    'Uncertainty',
    'VesselPlot',
    'GridDataPlot',
    'TriangDataPlot',
#    'Error2DLine',
    'update',
    'extend',
    'get',
    'whichclass',
    'module_path',
    'read_config',
    'errorplot',
    'cumsum_quantiles',
    'contours',
    'point_hull',
    'legend',
]











