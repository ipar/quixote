import os
import sys
import numpy as np
import functools
import logging

slog = logging.getLogger('quixote')

from .utils import *


def template_decorator(function=None, *, added_kw=None):
## Alternative for any number of kwargs
#def template_decorator(function=None, *dummy, **added_kw):
    ''''Docstring for decorator.

    Description of this decorator

    Parameters
    ----------
    function : types.FunctionType or None, optional
        the function to be decoratored
        if None, then returns decorator to apply.
    kw1, kw2 : any, optional
        key-word only arguments

    Returns
    -------
    wrapper : types.Function
        wrapper for function
        does a few things

    '''
    if function is None: # allowing for optional arguments
        return functools.partial(template_decorator, added_kw=added_kw)

    @functools.wraps(function)
    def wrapper(*args, **kw):
        """Wrapper docstring.

        This docstring is not visible.
        But is an excellent place to add some internal documentation.

        """
        # do stuff here
        return_ = function(*args, **kw)
        # and here

        ## Alternative, if added_kwargs are to be added to func.
        #xkw ={**kw}
        #xkw.update(added_kw)
        #return_ = function(*args, **xkw)
        return return_
    # /def

    ## Things to be done to the wrapper

    return wrapper
# /def


## Does not work yet, I think
#class TemplateDecorator:
#    """
#    It works with the same @ sugar>
#    @TemplateDecorator
#    def z(self): pass
#    Now learn how to put kwargs in decorator
#    """
#    def __init__(self, function):
#        self.function = function
#        functools.update_wrapper(self, function)
#    def __call__(self, *args, **kwargs):
#        #Do stuff to function 
#        result = self.function(*args, **kwargs)
#        #Do stuff after function
#        return result







def try_block(func):
    """
    Tries self._ATTR and, if it is not there, then computes FUNC,
    which should create self._ATTR and returns it.
    Else, it returns self._ATTR.

    Additionally, after it has successfully implemented ._ATTR, if it is
    a grid attribute, it will be flipped in the USN case.
    LSN should remain unchanged.
    """
    @functools.wraps(func)
    def wrapped(self, *args, **kwargs): ##Args and Kwargs not used, should I pass them to func??
        try:
            return getattr(self, '_'+func.__name__)
        except:
            try:
                func(self) #Func should create self._variable
                return getattr(self, '_'+func.__name__)
            except Exception:
                self.log.exception("'{}' could not be set".format(func.__name__))
                #setattr(self, '_'+func.__name__, None)
                #return getattr(self, '_'+func.__name__)
                #return None
                raise Exception
    return wrapped


def solps_property(func):
    """ Sums up both try_block and property in a single decorator"""
    return property(try_block(func))


def solps_property2(func):
    """ If func is given and not successful, then tries 
    """
    @functools.wraps(func)
    def wrapped(self, *args, **kwargs): ##Args and Kwargs not used, should I pass them to func??
        try:
            return getattr(self, '_'+func.__name__)
        except:
            try:
                func(self) #Func should create self._variable. Needs args and kwargs??
                return getattr(self, '_'+func.__name__)
            except:
                pass
            try:
                self._read_signal('_'+func.__name__, func.__name__)
                return getattr(self, '_'+func.__name__)
            except:
                pass

            self.log.exception("'{}' could not be set".format(func.__name__))
            #setattr(self, '_'+func.__name__, None)
            #return getattr(self, '_'+func.__name__)
            #return None
            raise Exception
    return property(wrapped)



def postmortem(func):
    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        try:
            func(*args,**kwargs)
        except:
            import ipdb, traceback, sys
            typ, value, tb = sys.exc_info()
            traceback.print_exc()
            ipdb.post_mortem(tb)
    return wrapped




class Property(object):
    """
    Emulate PyProperty_Type() in Objects/descrobject.c
    by Raymond Hettinger.
    """

    def __init__(self, fget=None, fset=None, fdel=None, doc=None):
        self.fget = fget
        self.fset = fset
        self.fdel = fdel
        if doc is None and fget is not None:
            doc = fget.__doc__
        self.__doc__ = doc

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self
        if self.fget is None:
            raise AttributeError("unreadable attribute")
        return self.fget(obj)

    def __set__(self, obj, value):
        if self.fset is None:
            raise AttributeError("can't set attribute")
        self.fset(obj, value)

    def __delete__(self, obj):
        if self.fdel is None:
            raise AttributeError("can't delete attribute")
        self.fdel(obj)

    def getter(self, fget):
        return type(self)(fget, self.fset, self.fdel, self.__doc__)

    def setter(self, fset):
        return type(self)(self.fget, fset, self.fdel, self.__doc__)

    def deleter(self, fdel):
        return type(self)(self.fget, self.fset, fdel, self.__doc__)




## Does not really work, since function is not activated until after returned
## Maybe cooler if inside of metaclass taht can pass cls and thus
## Know how deep we are, the attached methods, etc.
#def debugger(func):
#    dlog = create_log('DLOG', level='debug')
#
#    dlog.debug("Entering '{}' decorator".format(func.__name__))
#
#    @functools.wraps(func)
#    def wrapped(*args, **kwargs):
#        dlog.debug("Entering '{}'".format(func.__name__))
#        return_ = func(*args, **kwargs)
#        dlog.debug("Leaving  '{}'".format(func.__name__))
#        return return_
#
#    dlog.debug("Leaving  '{}' decorator".format(func.__name__))
#    return wrapped





