import logging
import numpy as np
import scipy.interpolate as itp

from .utils import extend
#from .utils import *
#from .decorators import *

slog = logging.getLogger('quixote')




## ============== MAIN OUTPUT FILES PARSERS ===================================

def b2(b2file, var, dims=None):
    data = []
    with open(b2file, 'r') as f:
        is_body = False
        for i, line in enumerate(f):
            pieces = line.split()
            if (pieces[0] == '*cf:' and
                pieces[3].lower() == var.lower() and
                int(pieces[2]) > 0):
                i_header = i
                nentry = int(pieces[2])
                is_body = True
                continue
            if is_body:
                #pieces = line.split()
                for piece in pieces:
                    data.append(float(piece))
                if len(data) == nentry: break
    if dims:
        return np.array(data).reshape(dims, order = 'F')
    else:
        return np.array(data)


def fort46(f46, var):
    """
    Eirene output for SOLPS-ITER in the triangular mesh.
    """

    with open(f46,'r') as fd:
        ncvtot = int(fd.readline().split()[0])
    return fort(f46, var, ncvtot)

#def fort44(f44, var):
#    """
#    """
#    with open(f44,'r') as fd:
#        tmp = fd.readline().split()
#        if len(tmp) == 3:
#            gdim = int(tmp[0])
#        elif len(tmp) == 4:
#            nx = int(tmp[0])
#            ny = int(tmp[1])
#            gdim = (nx,ny)
#    return fort(f44, var, gdim)

def fort44(f44, var, gdims=None, infere_dims=True):
    """
    """
    with open(f44,'r') as fd:
        tmp = fd.readline().split()
        if not infere_dims:
            gdim = False
        elif not gdims and len(tmp) == 3:
            gdim = int(tmp[0])
        elif not gdims and len(tmp) == 4:
            nx = int(tmp[0])
            ny = int(tmp[1])
            gdim = (nx,ny)
    return fort(f44, var, gdim)

#def fortb2(fortfile, var, gdims=False, squeeze=False): ##NAME??
def fort(fortfile, var, gdims=False, squeeze=False):
    data = []
    with open(fortfile,'r') as f:
        is_body = False
        for i, line in enumerate(f):
            pieces = line.split()
            wh_header = (
                pieces[0] ==  '*eirene' and
                var.lower() in pieces)
            if wh_header:
                i_header = i
                is_body = True
                nentries = int(pieces[-1])
                continue
            if is_body:
                for piece in pieces:
                    data.append(float(piece))
                if len(data) == nentries:
                    break
    if gdims:
        edims = nentries//np.prod(gdims)
        if squeeze and edims == 1:
            dims = gdims
        else:
            dims = extend(gdims, edims)
        return np.array(data).reshape(dims, order = 'F')
    else:
        return np.array(data)



##ATTENTION: CHanged to infere_dims.
## If infere_dims = False, then maybe enough
#def fort44_special(f44, var):
#    """
#    """
#    with open(f44,'r') as fd:
#        tmp = fd.readline().split()


def f90namelist(namelist):
    """ It works, but it doesn't like things like
    userfluxparm(1,1) = bunch of numbers.
    It wants userfluxparm(:,1) = bunch of numbers.
    """
    try:
        import f90nml
    except:
        log.exception("Python module f90nml required.")
    f90parser = f90nml.Parser()
    #parser.default_start_index = 1 ## Default, but maybe zero??
    return f90parser.read(namelist)




## ============== STRUCTURES / INPUTS PARSERS =================================

def structure(structurefile):
    with open(structurefile, 'r') as fs:
        nstructs = int(fs.readline())
        structs = []
    
        body = False
        header = False
        for line in fs:
            if line.startswith('Structure'):
                body = False
                header = True
                continue
            if header:
                nsurfs = np.abs(int(line))
                rz = np.full((nsurfs,2), np.nan)
                i =0
                body = True
                header = False
                continue
            if body:
                tmp = line.split()
                rz[i,0] = float(tmp[0])
                rz[i,1] = float(tmp[1])
                i+=1
                if i == nsurfs:
                    structs.append(rz)
                    body = False
                    continue
    
        if len(structs) != nstructs:
            print("Error in number of structures")
    return structs










## ============== PSI / EQUILIBRIUM PARSERS ===================================

def rzpsi(rzpsifile):
    """ Add rzpsi.dat to the list of findable files.
    """
    r = []
    z = []
    psi = []
    with open(rzpsifile) as fd:
        fd.readline()
        tmp = fd.readline().split()
        nr = int(tmp[0])
        nz = int(tmp[1])
        npsi = nr*nz

        fd.readline()
        fd.readline()
        for line in fd:
            tmp = line.split()
            for val in tmp:
                r.append(float(val))
            if len(r) == nr:
                break

        fd.readline()
        fd.readline()
        for line in fd:
            tmp = line.split()
            for val in tmp:
                z.append(float(val))
            if len(z) == nz:
                break

        fd.readline()
        fd.readline()
        for line in fd:
            tmp = line.split()
            for val in tmp:
                psi.append(float(val))
            if len(psi) == npsi:
                break

    #return np.array(r), np.array(z), np.array(psi).reshape((nr,nz))
    return {'r': np.array(r), 'z': np.array(z), 
            'psi': np.array(psi).reshape((nr,nz))}





def equ(eqfile):
    data = {}
    with open(eqfile, 'r') as eqf:
        for _ in range(10):
            eqf.readline()
        data['jm'] = int(eqf.readline().split()[-2])
        data['km'] = int(eqf.readline().split()[-2])
        data['psib'] = float(eqf.readline().split()[-2])
        data['btf'] = float(eqf.readline().split()[-2])
        data['rtf'] = float(eqf.readline().split()[-2])

        for _ in range(2):
            eqf.readline()

        r = []
        for line in eqf:
            tmp = line.split()
            for val in tmp:
                r.append(float(val))
            if len(r) == data['jm']:
                break
        data['r'] = np.array(r)

        for _ in range(2):
            eqf.readline()
        
        z = []
        for line in eqf:
            tmp = line.split()
            for val in tmp:
                z.append(float(val))
            if len(z) == data['km']:
                break
        data['z'] = np.array(z)

        for _ in range(2):
            eqf.readline()
        
        psimb = []
        npsi = data['jm']*data['km']
        for line in eqf:
            tmp = line.split()
            for val in tmp:
                psimb.append(float(val))
            if len(psimb) == npsi:
                break
        ## plt.contourf(data['r'], data['z'], data['psi']) shows correct shape
        ## But what values??
        data['psi'] = np.array(psimb).reshape((data['jm'],data['km']))+data['psib']

        data['psio'] = np.nanmax(data['psi']) ## As long as psi_sep is substracted.
        inds = np.where(data['psi'] == np.nanmax(data['psi']))
        data['zo'] = data['z'][inds[0]][0]
        data['ro'] = data['r'][inds[1]][0]
        data['mpsi'] = (data['psi']-data['psio'])/(data['psib']-data['psio']) 

        data['mpsi_spl'] = itp.RectBivariateSpline(
            data['r'], data['z'], data['mpsi'].T, s=0.0)

        return data




def gfile(filename):
    """
    By Robert Willcox in SOLPSxport utils.
    Copied here for convenience.
    """
    infile = open(filename, 'r')
    lines = infile.readlines()

    # read first line for case string and grid size
    line = lines[0]
    words = line.split()

    nw = int(words[-2])
    nh = int(words[-1])
    psi = np.linspace(0, 1, nw)

    # read in scalar parameters
    #   note: word size of 16 characters each is assumed for
    #   all of the data to be read

    # line 1
    line = lines[1]
    rdim = float(line[0:16])
    zdim = float(line[16:32])
    rcentr = float(line[32:48])
    rleft = float(line[48:64])
    zmid = float(line[64:80])

    # line 2
    line = lines[2]
    rmaxis = float(line[0:16])
    zmaxis = float(line[16:32])
    simag = float(line[32:48])
    sibry = float(line[48:64])
    bcentr = float(line[64:80])

    # line 3
    line = lines[3]
    current = float(line[0:16])

    # read in profiles
    #   start by reading entire file into single list theni
    #   split into individual profiles
    #   first block has 5 profiles of length nw and one array of length nh*nw

    temp = []
    count = 0
    lnum = 5
    terms = 5 * nw + nw * nh
    while count < terms:
        line = lines[lnum]
        numchar = len(line)
        nwords = numchar // 16
        count1 = 0
        while count1 < nwords:
            i1 = count1 * 16
            i2 = i1 + 16
            temp.append(float(line[i1:i2]))
            count1 += 1
            count += 1
        lnum += 1

    fpol = temp[0:nw]
    pres = temp[nw:2 * nw]
    ffprime = temp[2 * nw:3 * nw]
    pprime = temp[3 * nw:4 * nw]
    psirz_temp = temp[4 * nw:(4 + nh) * nw]
    qpsi = temp[(4 + nh) * nw:]

    # split psirz up into 2D matrix
    count = 0
    psirz = []
    while count < nh:
        ind1 = count * nw
        ind2 = ind1 + nw
        psirz.append(psirz_temp[ind1:ind2])
        count += 1

    # scalars for length of boundary and limiter arrays
    line = lines[lnum]
    words = line.split()
    nbbbs = int(words[0])
    limitr = int(words[1])

    # read boundary and limiter points into temp array

    temp = []
    count = 0
    terms = 2 * (nbbbs + limitr)
    lnum += 1
    while count < terms:
        line = lines[lnum]
        numchar = len(line)
        nwords = numchar // 16
        count1 = 0
        while count1 < nwords:
            i1 = count1 * 16
            i2 = i1 + 16
            temp.append(float(line[i1:i2]))
            count1 += 1
            count += 1
        lnum += 1
    bdry_temp = temp[0:(2 * nbbbs)]
    limit_temp = temp[(2 * nbbbs):]

    # split boundary into (R,Z) pairs
    count = 0
    rbdry = []
    zbdry = []
    while count < len(bdry_temp) - 1:
        rbdry.append(bdry_temp[count])
        zbdry.append(bdry_temp[count + 1])
        count += 2

    # split limiter into (R,Z) pairs
    count = 0
    rlim = []
    zlim = []
    while count < len(limit_temp) - 1:
        rlim.append(limit_temp[count])
        zlim.append(limit_temp[count + 1])
        count += 2

    g = dict(nw=nw, nh=nh, rdim=rdim, zdim=zdim, rcentr=rcentr, rleft=rleft,
        zmid=zmid, rmaxis=rmaxis, zmaxis=zmaxis, simag=simag, sibry=sibry,
        current=current, fpol=np.array(fpol), ffprime=np.array(ffprime),
        pprime=np.array(pprime), psirz=np.array(psirz), qpsi=np.array(qpsi),
        nbbbs=nbbbs, bcentr=bcentr, pres=np.array(pres), limitr=limitr,
        rbdry=np.array(rbdry), zbdry=np.array(zbdry), rlim=np.array(rlim),
        zlim=np.array(zlim))

    #g = loadg(gfile)
    g['mpsi'] = (g['psirz'] - g['simag']) / (g['sibry'] - g['simag'])
    g['dr'] = g['rdim'] / (g['nw'] - 1)
    g['dz'] = g['zdim'] / (g['nh'] - 1)
    g['r'] = g['rleft'] + np.arange(0,g['nw'])*g['dr']
    g['z'] = g['zmid'] - 0.5*g['zdim'] + np.arange(0,g['nh'])*g['dz']

    return g






## ============== PARSERS OF ATOMIC AND MOLECULAR DATABASES PARSERS ===========

def adasf11(path):
    log.exception("ADAS11 not yet implemented.")


def adasf15(path):
    """ Reads ADAS f15 and returns dicts with the parsed information.
    path: str
        Path to the .dat file containing the ADAS f15 formatted data.

    Returns
    -------
    dict
        The dictionary contains: 
        'nsel':
            Total amount of reactions in the file

        'label':
            Description in first line of file.


        'wl': [nsel]
            Wavelength of the reaction [A].

        'ndens': [nsel]
            Number of points in the density discretization [cm-3].

        'nte': [nsel]
            Number of points in the temperature discretization.

        'type': [nsel]
            Type of reaction

        'isel': [nsel]
            Number of the reaction in the file.


        'pec': [nsel](ndens, nte)
            Photon Emissivity Coefficients [photon cm-3 s-1].

        'te': [nsel](nte)
            Electron temperature.

        'dens": [nsel](ndens)
            Density.
    """
    data = {}
    log.info("Using '{}'".format(path))
    with open(path, 'r') as file:
        line = file.readline()
        data['nsel'] = int(line[:5])
        data['label'] = line[6:].strip()
        log.info("Label: {}".format(data['label']))
        log.info("Number of transitions, nsel = {}".format(data['nsel']))

        data['wl'] = []
        data['ndens'] = []
        data['nte'] = []
        data['type'] = []
        data['isel'] = []
        data['pec'] = []
        data['te'] = []
        data['dens'] = []

        isel = 0
        while isel<data['nsel']:
            header = file.readline().split('/')
            if header[-1].split('=')[0].strip().lower() == 'isel':
                tmp = header[0].replace('A','').split()
                data['wl'].append(float(tmp[0]))
                data['ndens'].append(int(tmp[1]))
                data['nte'].append(int(tmp[2]))
                data['type'].append(header[-3].split('=')[1].strip().lower())
                nete = []
                for line in file:
                    nete = extend(nete, [float(ele) for ele in line.split()])
                    if len(nete) >= data['ndens'][isel]+data['nte'][isel]:
                        break
                data['dens'].append( np.array(nete[:data['ndens'][isel]])) 
                data['te'].append( np.array(nete[-data['nte'][isel]:])) 
                mxdim = data['ndens'][isel]*data['nte'][isel]
                tmp = []
                for line in file:
                    tmp = extend(tmp, [float(ele) for ele in line.split()])
                    if len(tmp) >= mxdim:
                        break
                data['pec'].append(np.array(tmp).reshape(
                    (data['ndens'][isel], data['nte'][isel])))

                if False:
                    data['dens'] *= dfactor
                    data['pec'] *= dfactor 
                    data['wl'] *= wlfactor

                isel+=1

        return data



