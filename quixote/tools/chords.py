import os
import numpy as np
import logging
from scipy.spatial import ConvexHull

from .utils import *
from .decorators import *

slog = logging.getLogger('quixote')





class Line(object):
    """A line created from two points."""
    def __init__(self, start, secondary, ltype='points'):
    # If numpy array is passed, then work with it too  
        if ltype=='point' or ltype=='points':
            self.start = np.array(start)
            self.end   = np.array(secondary)
            self.direction   = self.end - self.start
            self.ltype = 'points'
        elif ltype=='direction' or ltype=='dir': 
            self.start = np.array(start)
            self.direction   = np.array(secondary)
            self.end   = self.start + self.direction
            self.ltype = 'direction'
        else:
            slog.error(
            "'ltype' must be either 'points' or 'direction', not '{}'"
            .format(ltype))

    def point(self, l):
        """Returns the (r,z,phi) coordinates of the point on the line.

        Parameters
        ----------
        l : float
            Position along the line, l==0 at the starting point
            of the line and l==1 at the endpoint of the line.
        """
        # The points defining the line:
        rc, zc, pc = self.start
        rd, zd, pd = self.end

        cc  = np.cos(pc*np.pi/180.0)
        sc  = np.sin(pc*np.pi/180.0)
        cd  = np.cos(pd*np.pi/180.0)
        sd  = np.sin(pd*np.pi/180.0)

        # Cartesian coordinates:
        x = rc*cc + l*(rd*cd-rc*cc)
        y = rc*sc + l*(rd*sd-rc*sc)
        z = zc    + l*(zd-zc)

        # Zylindrical coordinates:
        r = np.sqrt(x**2+y**2)
        z = z
        p = np.arctan2(x,y) * 180.0/np.pi # --> in degrees

        return np.array([r,z,p])












#class Intersection(object):
#    """ Class which contains the position and some other
#    properties of an intersection of a LOS with the grid
#    """
#    def __init__(self, r, z, p, l, cells):
#        #import ipdb; ipdb.set_trace()
#        if not isinstance(cells, list): #Originally it was cells[0] not a list, so it is a list of lists?
#            cells = [cells]      # Should be a list of [nx,ny] lists of
#        self.cells = cells       # all associated / neighboring cells
#        self.r = r
#        self.z = z
#        self.p = p
#        self.l = round(l,12) # Distance along the line of sight
#        # (l==0 at LOS start, l==1 at LOS end)
#        # (Rounding is necessary to be able to compare
#        #  and sort the intersections later.)
#
#    def combine(self, intersection):
#        self.cells.extend(intersection.cells)



class Intersection(object):
    """ Class which contains the position and some other
    properties of an intersection of a LOS with the grid
    cells is the list of all associated neighbours.
    l is lengh along LOS, 0<=l<=1
    """
    def __init__(self, r, z, p, l, cells):
        ## Maybe use tuples instead?? They cannot be expanded wrongly, right?
        if not isinstance(cells, list):
            cells = [cells]
        self.cells = cells 
        self.r = r
        self.z = z
        self.p = p
        self.l = round(l,12)

    def combine(self, intersection):
        self.cells.extend(intersection.cells)


#class LineSegment(object):
#    def __init__(self, start, end, ici):
#        self.start  = np.array(start)
#        self.end    = np.array(end)
#        self.ici     = ici
#        self.seg    = np.vstack([self.start,self.end])
#        self.length = distance_cylindrical_coordinates(self.start, self.end)


class LineSegment(object):
    def __init__(self, start, end, icell):
        self.start = np.array(start)
        self.end = np.array(end)
        self.icell = icell
        self.seg = np.vstack([self.start,self.end])
        self.length = distance_cylindrical_coordinates(self.start, self.end)



def find_intersection(line1, line2):
    """ Returns the intersection point of two lines line1 and line2.
    """
    Ax = line1.start[0]
    Ay = line1.start[1]
    ax = line1.direction[0]
    ay = line1.direction[1]

    Bx = line2.start[0]
    By = line2.start[1]
    bx = line2.direction[0]
    by = line2.direction[1]

    if ax*by - ay*bx == 0: # Parallel lines, no intersection!
        return None
    t  = ( (Bx-Ax)*ay - (By-Ay)*ax ) / ( ax*by - ay*bx )

    return np.array([Bx+t*bx,By+t*by])



def point_distance(point1, point2):
    p1 = np.array(point1)
    p2 = np.array(point2)
    return np.linalg.norm(p2-p1)



def points_on_same_side_of_line(point1, point2, line):
    """ Given a line, return True if point1 and point2 are on the same
    side of such line, and False if they are in opposite sides.
    """
    pline = Line(point1, point2, ltype='point')
    # Find the intersection:
    # (or how much pline needs to be extended to the intersection)
    Ax = line.start[0]
    Ay = line.start[1]
    ax = line.direction[0]
    ay = line.direction[1]
    Bx = pline.start[0]
    By = pline.start[1]
    bx = pline.direction[0]
    by = pline.direction[1]

    if (ax*by - ay*bx) == 0.0:
        return True #line and pline are parallel.
    t  = ( (Bx-Ax)*ay - (By-Ay)*ax ) / ( ax*by - ay*bx )
    if 0.000001 < t < 0.999999: # (If the point is on the line, numerical
        return False          # imprecision might result in the wrong
    else:                     # outcome when using 0 < t < 1 ...)
        return True




def point_in_cell(cell, point):
    """Returns True if the point is inside of the cell.
    """
    # cell must be a run.grid[nx,ny] array

    # Cell Center:
    x = np.average(cell[:,0])
    y = np.average(cell[:,1])
    cc = np.array([x,y])

    # Cell Faces:
    cf = []
    for i in range(cell.shape[0]):
        if i == 0:
            face = Line(cell[-1],cell[i])
        else:
            face = Line(cell[i-1],cell[i])
        cf.append(face)
    for face in cf:
        if not points_on_same_side_of_line(cc,point,face):
            return False
    return True





def distance_cylindrical_coordinates(start, end):
    """Calculate the distance between two
    points given in cylindrical coordinates."""
    # Convert to cartesian coordinates:
    x = (end  [0]*np.cos(end  [2]*np.pi/180.0)-
         start[0]*np.cos(start[2]*np.pi/180.0))
    y = (end  [0]*np.sin(end  [2]*np.pi/180.0)-
         start[0]*np.sin(start[2]*np.pi/180.0))
    z =  end  [1]   -    start[1]
    return np.linalg.norm(np.array([x,y,z]))






def parse_los_file(path):
    """
    """
    los = {}
    with open(path,'r') as fl:
        style = ''
        iblock=0
        bundle_name = fl.readline().split()[0]

        for i, line in enumerate(fl):

            # Skip empty and comment lines:
            try:
                if line.split()[0].strip().startswith('#'):
                    continue
            except:
                continue


            ## HEAD
            if not 'From' in line and not 'To' in line and iblock == 0:
                tmp = line.split('(')
                loshead = tmp[0].strip()
                if len(tmp)>1:
                    style = 'diaggeom'
                else:
                    style = 'free'
                    iblock+=1


            ## BODY
            #AUG STYLE
            elif style == 'diaggeom':
                r = float(line.split('R=')[1].split('m,')[0])
                z = float(line.split('z=')[1].split('m,')[0])
                p = float(line.split('phi=')[1])
                if 'From' in line:
                    losnumber = line.split()[0].replace('.','')
                    if losnumber == 'From':
                        losnumber = ''
                    start = np.array([r,z,p]).astype(float)
                elif 'To' in line:
                    end = np.array([r,z,p]).astype(float)

                    #New diaggeom .coords format
                    if losnumber:
                        losname = loshead+'-'+losnumber
                    # Old diaggeom .coords format
                    else:
                        losname = loshead.split('0')
                        if losname[-1] == '': # Ends with 0 (e.g. RIN010)
                            losname = losname[0]+'-'+losname[-2]+'0'
                        else:
                            losname = losname[0]+'-'+losname[-1]

                    los[losname] = [start, end]
                    iblock=0

            #FREE STYLE
            elif style == 'free':
                try:
                    r, z, p = line.split(',')
                except:
                    try:
                        r = float(line.split('R=')[1].split('m,')[0])
                        z = float(line.split('z=')[1].split('m,')[0])
                        p = float(line.split('phi=')[1])
                    except:
                        slog.error("Error reading LOS "+
                        "'{0}' from '{1}' in line {2}.".format(
                        loshead,  os.path.basename(path), i))

                if iblock == 1:
                    start = np.array([r,z,p]).astype(float)
                    iblock+=1
                elif iblock == 2:
                    end = np.array([r,z,p]).astype(float)
                    los[loshead] = [start, end]
                    iblock = 0
                else:
                    slog.error("For than points found for LOS "+
                    "'{0}' from '{1}' in line {2}.".format(
                    loshead,  os.path.basename(path), i))
    return los











def point_hull(cell, point):
    """If in face, it is also True
    """
    hull = ConvexHull(np.vstack([cell, point]), qhull_options='Qc')
    if len(cell) in hull.vertices:
        return 'out', []
    elif len(hull.coplanar>0):
        cop = hull.coplanar[0]
        cop = np.setdiff1d(cop, len(cell))
        return 'face', np.vstack([cell[cop[0]], cell[cop[1]]])
        ## Maybe return indices of face points, since geometry of faces is always the same?
    else:
        return 'in', []


def rzeval_hull(mother, var, point):
    """
    """
    if isinstance(var, str):
        var = get(mother,var)
    if mother.grid_type == 'structured':
        for ix in range(mother.nx):
            for iy in range(mother.ny):
                loc, _ = point_hull(mother.grid[ix, iy], point)
                if loc == 'in':
                    return var[ix,iy]
                elif loc == 'face':
                    ## Get which face
                    return var[ix,iy]
        return np.nan
    elif mother.grid_type == 'unstructured':
        for ici in range(mother.nci):
            loc, _ = point_hull(mother.grid[ici], point)
            if loc == 'in':
                return var[ix,iy]
            elif loc == 'face':
                ## Get which face
                return var[ix,iy]
        return np.nan
    else:
        mother.log.exception("grid_type must be either structured or unstructured")




