
import os
import re
import sys
import gzip
import itertools
import subprocess
import numpy as np
from functools import wraps
import collections
from collections import OrderedDict, deque, defaultdict
from collections.abc import Callable
import logging
import copy
import yaml



from .core import *
from .decorators import *

slog = logging.getLogger('quixote')













## ATTENTION: def yield_dataframe instead??
#def df_generator(df, order='rc'):
def df_generator(df, order='rc'):
    """ rc is row -> columns and cr is columns -> rows."""
    if order.lower() == 'rc':
        for row in df.iterrows():
            for itm in row[1].items():
                yield row[0], itm[0], itm[1]
    elif order.lower() =='cr':
        #for col in df.items():
        #    for itm in row[1].items():
        #        yield row[0], itm[0], itm[1]
        pass #Not yet implemented

    else:
        raise Exception("Order must be either 'rc' or 'cr'")





def parse_dataframe(df,criterion):
    """Parses a pandas DataFrame for entries following certain criterion.

    For computational reasons, it divides the criterions in those
    affecting numbers and those affecting python objects, but the final
    result gets put together again.

    Parameters
    ----------
    df: pandas.DataFrame type
        The data frame to be parsed

    criterion: dict
        Dictionary which keys correspond to labels and which values correspond
        to a single criterion for each value.
        Dictionaries with multiple values per key are not allowed.


    Results
    -------
    pandas DataFrame object, which entries correspond to df entries
    that follow criterion.
    
    Maybe raise warning when one of the entries is !Lambda, or lambda,
    since that means that run has not yet been initialized and thus
    it should not be used for matching patterns.
    """
    import pandas as pd
    cnames = list(criterion)
    cseries = pd.Series(criterion)
    tmp = df[cnames]
    tmp_num = tmp.select_dtypes(exclude=['object'])
    tmp_obj = tmp.select_dtypes(include=['object'])
    cnames_num = list(tmp_num)
    cseries_num = pd.to_numeric(cseries[cnames_num])
    cnames_obj = list(tmp_obj)
    cseries_obj = cseries[cnames_obj]
    try:
        mask_num = np.isclose(tmp_num,cseries_num)
        tmp_num = tmp_num.where(mask_num)
    except:
        pass

    ## If unloaded lambda or function, warn about it.
    try:
        log = create_log('tools', {'ident': 'parse_dataframe'})
        import types
        for itms in df_gen(tmp_obj):
            if isinstance(itms[2], types.LambdaType):
                try:
                    log.warning(
                    "For ID '{0}', value of '{1}' is lambda or function.".format(
                        df.iloc[itms[0]]['id'],itms[1]))
                    log.warning("Lambda or functions cannot be used to filter "+ 
                    "at creation stage, because SOLPS simulations have not "+
                    "been loaded yet.")
                except:
                    log.warning(
                    "For ID '{0}', value of '{1}' is lambda or function.".format(
                        df.itms[0],itms[1]))
                    log.warning("Lambda or functions cannot be used to filter "+ 
                    "at creation stage, because SOLPS simulations have not "+
                    "been loaded yet.")

    except:
        pass


    try:
        mask_obj = (tmp_obj == cseries_obj)
        tmp_obj = tmp_obj.where(mask_obj)
    except:
        pass
    tmp2 = pd.concat([tmp_num,tmp_obj], axis=1)
    tmp2 = tmp2.dropna()
    return df.iloc[tmp2.index]






def exp_cross_references(exp):
    """
    Substitutes arguments values beginning with $ with the corresponding
    reference.
    E.g.:
        id: $nshot
        nshot: 34314

        will result in
        id: 34314
        nshot: 34314

    Edit 2021: One can use & to add a tag before a value and then reference it
    by *. Eg: nshot: &nshot 34311 and then id: *nshot.

    TODO 2021: Allow (or create new function) that allows to replace quantity
    by shot quantity AFTER loading. So these should be used for ordering and not
    for including.
    """
    try:
        news = {}
        for k,v in list(exp['defaults'].items()):
            if isinstance(v, str) and v.startswith('$'):
                ref = v[1:]
                news[k] = exp[ref]
        exp['defaults'].update(news)

        news = {}
        for case in exp['cases']:
            for k,v in list(case.items()):
                if isinstance(v, str) and v.startswith('$'):
                    ref = v[1:]
                    news[k] = case[ref]
            case.update(news)

    except IOError as e:
        slog.exception("Error opening db file")
    except KeyError as e:
        slog.exception("Reference to non existing key field '{}'.".format(ref))
    except Exception as e:
        slog.exception("Yaml file contains unparseable statement.")

    return exp

## How is this different to previous??
def yaml_xrefs(yamldict):
    """
    Substitutes arguments values beginning with $ with the corresponding
    reference.
    E.g.:
        id: $nshot
        nshot: 34314

        will result in
        id: 34314
        nshot: 34314

    Notes
    -----
    They only work for same level references.
    """
    try:
        news = {}
        for k,v in list(yamldict.items()):
            if isinstance(v, str) and v.startswith('$'):
                ref = v[1:]
                news[k] = yamldict[ref]
        yamldict.update(news)
    except IOError as e:
        slog.exception("Error opening db file.")
    except KeyError as e:
        slog.exception("Reference to non existing key field '{}'.".format(ref))
    except Exception as e:
        slog.exception("Yaml file contains unparseable statement.")

    return yamldict




def yaml_runrefs(yamldict):
    """
    Allow syntax like:
        nesepm: *ne[omp,sep]
        so that after shot has been loaded, and
        >> shot.ne[shot.omp,shot.sep]
        >> 2.0e19
        then
        nesemp:2.0e19

        This will only serve for ordering if ordering is done
        after loading, which in principle shouldn't be a problem,
        as reordering of loaded cases wouldn't mean a reload,
        just a change of indexes.

        It would also require to match * with True when filtering
        through the database and, loading, reapplying the filtering.
    """




