import os
import re
import sys
import gzip
import itertools
import subprocess
import numpy as np
from scipy.interpolate import RegularGridInterpolator as RGI
from scipy.interpolate import interp2d
from functools import wraps
import collections
from collections import OrderedDict, deque, defaultdict
from collections.abc import Callable
import logging
import copy
import yaml




from .utils import *
from .decorators import *

slog = logging.getLogger('quixote')









def read_config(include_local=True):
    """ Reads default and local configuration files and merge them together
    with appropiate priority order.
    Improved so now it modifies a deep dict just fine.
    include_local = False to be used on test cases.
    """

    config = {}
    local  = {}

    # Read 'default.yaml' file:
    yaml_file = os.path.join(module_path(),'config/default.yaml')
    try:
        config = yaml_loader(yaml_file)
    except IOError:
        slog.exception(
        "Error opening 'default.yaml' file '{0}'".format(yaml_file))
    except:
        slog.exception(
        "Yaml file contains unparseable statement '{0}'".format(yaml_file))

    # Read 'local.yaml' file:
    if include_local:
        yaml_file = os.path.join(module_path(),'config/local.yaml')
        try:
            local = yaml_loader(yaml_file)
        except IOError as e:
            slog.info("No local Yaml file '{0}' is provided".format(yaml_file))
        except Exception as e:
            slog.exception(
            "Yaml file contains unparseable statement '{0}'".format(yaml_file))

    return update(config, local)



def b2_parser(b2file, var, dims):
    data = []
    with open(b2file, 'r') as f:
        is_body = False
        for i, line in enumerate(f):
            pieces = line.split()
            if (pieces[0] == '*cf:' and
                pieces[3].lower() == var.lower() and
                int(pieces[2]) > 0):
                i_header = i
                nentry = int(pieces[2])
                is_body = True
                continue
            if is_body:
                #pieces = line.split()
                for piece in pieces:
                    data.append(float(piece))
                if len(data) == nentry: break
    return np.array(data).reshape(dims, order = 'F')



#def fort_parser(fortfile, var, dims):
#    data = []
#    nentries = np.prod(dims)
#    with open(fortfile,'r') as f:
#        ##Unnecesary, and self is not known.
#        #for i in range(2+self.natm+self.nmol+self.nion):
#        #    f.readline()
#        is_body = False
#        for i, line in enumerate(f):
#            pieces = line.split()
#            wh_header = (pieces[0] ==  '*eirene' and
#                         #var in line.split() and
#                         var.lower() in pieces and
#                         pieces[-1] == str(nentries))
#            if wh_header:
#                i_header = i
#                is_body = True
#                continue
#            if is_body:
#                #pieces = line.split()
#                for piece in pieces:
#                    data.append(float(piece))
#                if len(data) == nentries: break
#    return np.array(data).reshape(dims, order = 'F')



## ATTENTION: Standarddize names
def rzpsi_parser(rzpsifile):
    """ Add rzpsi.dat to the list of findable files.
    """
    r = []
    z = []
    psi = []
    with open(rzpsifile) as fd:
        fd.readline()
        tmp = fd.readline().split()
        nr = int(tmp[0])
        nz = int(tmp[1])
        npsi = nr*nz

        fd.readline()
        fd.readline()
        for line in fd:
            tmp = line.split()
            for val in tmp:
                r.append(float(val))
            if len(r) == nr:
                break

        fd.readline()
        fd.readline()
        for line in fd:
            tmp = line.split()
            for val in tmp:
                z.append(float(val))
            if len(z) == nz:
                break

        fd.readline()
        fd.readline()
        for line in fd:
            tmp = line.split()
            for val in tmp:
                psi.append(float(val))
            if len(psi) == npsi:
                break

    return np.array(r), np.array(z), np.array(psi).reshape((nr,nz))




def structure_parser(structurefile):
    with open(structurefile, 'r') as fs:
        nstructs = int(fs.readline())
        structs = []
    
        body = False
        header = False
        for line in fs:
            if line.startswith('Structure'):
                body = False
                header = True
                continue
            if header:
                nsurfs = np.abs(int(line))
                rz = np.full((nsurfs,2), np.nan)
                i =0
                body = True
                header = False
                continue
            if body:
                tmp = line.split()
                rz[i,0] = float(tmp[0])
                rz[i,1] = float(tmp[1])
                i+=1
                if i == nsurfs:
                    structs.append(rz)
                    body = False
                    continue
    
        if len(structs) != nstructs:
            print("Error in number of structures")
    return structs



def fort46_parser(f46, var):
    """
    Eirene output for SOLPS-ITER in the triangular mesh.
    """

    with open(f46,'r') as fd:
        ncvtot = int(fd.readline().split()[0])
    return fort_parser(f46, var, ncvtot)



def fort44_parser(f44, var):
    """
    """
    with open(f44,'r') as fd:
        tmp = fd.readline().split()
        if len(tmp) == 3:
            gdim = int(tmp[0])
        elif len(tmp) == 4:
            nx = int(tmp[0])
            ny = int(tmp[1])
            gdim = (nx,ny)
    return fort_parser(f44, var, gdim)

def fort_parser(fortfile, var, gdims, squeeze=False):
    data = []
    with open(fortfile,'r') as f:
        is_body = False
        for i, line in enumerate(f):
            pieces = line.split()
            wh_header = (
                pieces[0] ==  '*eirene' and
                var.lower() in pieces)
            if wh_header:
                i_header = i
                is_body = True
                nentries = int(pieces[-1])
                continue
            if is_body:
                for piece in pieces:
                    data.append(float(piece))
                if len(data) == nentries:
                    break
    edims = nentries//np.prod(gdims)
    if squeeze and edims == 1:
        dims = gdims
    else:
        dims = extend(gdims, edims)
    return np.array(data).reshape(dims, order = 'F')






def module_path():
    dirpath = os.path.abspath(sys.modules['quixote'].__file__)
    return dirpath.rstrip(os.path.basename(dirpath))




def vessel_square(mother):
    east = np.max(mother.grid[0][:,0])
    west = np.min(mother.grid[0][:,0])
    north = np.max(mother.grid[0][:,1])
    south = np.min(mother.grid[0][:,1])
    for ici in range(1, mother.nci):
        tmp = np.max(mother.grid[ici][:,0])
        if tmp > east:
            east = tmp
        tmp = np.min(mother.grid[ici][:,0])
        if tmp < west:
            west = tmp
        tmp = np.max(mother.grid[ici][:,1])
        if tmp > north:
            north = tmp
        tmp = np.min(mother.grid[ici][:,1])
        if tmp < south:
            south = tmp
    vessel = np.zeros((4,4))
    vessel[:,0] = west,north,east,north
    vessel[:,1] = east,north,east,south
    vessel[:,2] = east,south,west,south
    vessel[:,3] = west,south,west,north

    return vessel




def intface(mother, centre):
    """ Interpolates centered quantity centre to all the faces.
    See utility/intface.F
    """
    tmp = np.full(mother.nfc, np.nan)
    fcvol = mother.fcvol
    fccv = mother.fccv
    for ifc in range(nfc):
        dv1 = fcvol[ifc,0]*centre[fccv[ifc,1]]
        dv2 = fcvol[ifc,1]*centre[fccv[ifc,0]]
        dvt = fcvol[ifc,0] + fcvol[ifc,1]
        tmp[ifc] = (dv1 + dv2) / dvt
    return tmp









##ATTENTION: ONLY works for rectangular grids.

## ATTENTION: Names are wrong, and it seems too specific to be here.
## Maybe in a new filed called tools_class.py or something like that.
## Also wrong for unstructured simulations.
def generate_mask(mother, **kwargs):
    """ Generates mask with given xrange and yrange, and extra dimensions.
    To generate more complex masks, use np.logical_or or np.logical_and
    on two basic masks.

    Parameters
    ----------
    xrange, yrange: int or list(int)
        Beginning and end of the range of the masked
        poloidal, radial indices.
        It follows the range() conventions on indices.
        Default to [0,nx], [0,ny] if ix=[], iy=[]

    ix, iy: int or list(int)
        Selected poloidal/radial indices to be
        included in the mask.
        xrange= [0,3] == ix=[0,1,2]
        Default to [].

    extra_dims : int or iterable(int)
        Expands mask from (nx,ny) to (nx,ny,extra_dims).
        Default to None.

    Returns
    -------
    mask: numpy.array with dtype=numpy.bool
        Array with True in the cells that dwell inside the desired
        subdivision of the grid.
        It has dimensions (nx,ny,extra_dims)
    """
    
    nx = mother.nx
    ny = mother.ny

    ix = priority('ix', kwargs, [])
    iy = priority('iy', kwargs, [])
    xrange = priority('xrange', kwargs, [])
    yrange = priority('yrange', kwargs, [])
    extra_dims = priority(['extra_dims', 'extra_dim', 'dims', 'dim'],
        kwargs, None)

    if not isinstance(ix, collections.abc.Iterable): ix = [ix]
    if not isinstance(ix, collections.abc.Iterable): iy = [iy]
    try:
        xrange = set(range(xrange[0], xrange[1]))
    except:
        pass
    try:
        yrange = set(range(yrange[0], yrange[1]))
    except:
        pass

    try:
        xrange = set(itertools.chain(xrange,ix))
    except:
        self.log.exception("xrange and ix could not be combined")
        return
        
    try:
        yrange = set(itertools.chain(yrange,iy))
    except:
        self.log.exception("yrange and iy could not be combined")
        return

    if not xrange: xrange = set(range(0, nx))
    if not yrange: yrange = set(range(0, ny))


    mask1 = np.zeros((nx,ny), dtype=bool)
    mask2 = np.zeros((nx,ny), dtype=bool)
    for i in range(nx):
        for k in range(ny):
            if (i in xrange):
                mask1[i,k] = True
            if (k in yrange):
                mask2[i,k] = True

    mask = np.logical_and(mask1, mask2)

    if extra_dims:
        if '__iter__' not in extra_dims.__class__.__dict__:
            extra_dims = [extra_dims]

        for dm in extra_dims:
            mask = np.repeat(mask.T[np.newaxis], dm, axis=0).T

    return mask






## ATTENTION: Standarddize names
def equ_parser(eqfile):
    data = {}
    with open(eqfile, 'r') as eqf:
        for _ in range(10):
            eqf.readline()
        data['jm'] = int(eqf.readline().split()[-2])
        data['km'] = int(eqf.readline().split()[-2])
        data['psib'] = float(eqf.readline().split()[-2])
        data['btf'] = float(eqf.readline().split()[-2])
        data['rtf'] = float(eqf.readline().split()[-2])

        for _ in range(2):
            eqf.readline()

        r = []
        for line in eqf:
            tmp = line.split()
            for val in tmp:
                r.append(float(val))
            if len(r) == data['jm']:
                break
        data['r'] = np.array(r)

        for _ in range(2):
            eqf.readline()
        
        z = []
        for line in eqf:
            tmp = line.split()
            for val in tmp:
                z.append(float(val))
            if len(z) == data['km']:
                break
        data['z'] = np.array(z)

        for _ in range(2):
            eqf.readline()
        
        psimb = []
        npsi = data['jm']*data['km']
        for line in eqf:
            tmp = line.split()
            for val in tmp:
                psimb.append(float(val))
            if len(psimb) == npsi:
                break
        ## plt.contourf(data['r'], data['z'], data['psi']) shows correct shape
        ## But what values??
        data['psi'] = np.array(psimb).reshape((data['jm'],data['km']))+data['psib']

        return data












def parse_gfile(filename):
#def loadg(filename):
    """
    By Robert Willcox in SOLPSxport utils.
    Copied here for convenience.
    """
    infile = open(filename, 'r')
    lines = infile.readlines()

    # read first line for case string and grid size
    line = lines[0]
    words = line.split()

    nw = int(words[-2])
    nh = int(words[-1])
    psi = np.linspace(0, 1, nw)

    # read in scalar parameters
    #   note: word size of 16 characters each is assumed for
    #   all of the data to be read

    # line 1
    line = lines[1]
    rdim = float(line[0:16])
    zdim = float(line[16:32])
    rcentr = float(line[32:48])
    rleft = float(line[48:64])
    zmid = float(line[64:80])

    # line 2
    line = lines[2]
    rmaxis = float(line[0:16])
    zmaxis = float(line[16:32])
    simag = float(line[32:48])
    sibry = float(line[48:64])
    bcentr = float(line[64:80])

    # line 3
    line = lines[3]
    current = float(line[0:16])

    # read in profiles
    #   start by reading entire file into single list theni
    #   split into individual profiles
    #   first block has 5 profiles of length nw and one array of length nh*nw

    temp = []
    count = 0
    lnum = 5
    terms = 5 * nw + nw * nh
    while count < terms:
        line = lines[lnum]
        numchar = len(line)
        nwords = numchar // 16
        count1 = 0
        while count1 < nwords:
            i1 = count1 * 16
            i2 = i1 + 16
            temp.append(float(line[i1:i2]))
            count1 += 1
            count += 1
        lnum += 1

    fpol = temp[0:nw]
    pres = temp[nw:2 * nw]
    ffprime = temp[2 * nw:3 * nw]
    pprime = temp[3 * nw:4 * nw]
    psirz_temp = temp[4 * nw:(4 + nh) * nw]
    qpsi = temp[(4 + nh) * nw:]

    # split psirz up into 2D matrix
    count = 0
    psirz = []
    while count < nh:
        ind1 = count * nw
        ind2 = ind1 + nw
        psirz.append(psirz_temp[ind1:ind2])
        count += 1

    # scalars for length of boundary and limiter arrays
    line = lines[lnum]
    words = line.split()
    nbbbs = int(words[0])
    limitr = int(words[1])

    # read boundary and limiter points into temp array

    temp = []
    count = 0
    terms = 2 * (nbbbs + limitr)
    lnum += 1
    while count < terms:
        line = lines[lnum]
        numchar = len(line)
        nwords = numchar // 16
        count1 = 0
        while count1 < nwords:
            i1 = count1 * 16
            i2 = i1 + 16
            temp.append(float(line[i1:i2]))
            count1 += 1
            count += 1
        lnum += 1
    bdry_temp = temp[0:(2 * nbbbs)]
    limit_temp = temp[(2 * nbbbs):]

    # split boundary into (R,Z) pairs
    count = 0
    rbdry = []
    zbdry = []
    while count < len(bdry_temp) - 1:
        rbdry.append(bdry_temp[count])
        zbdry.append(bdry_temp[count + 1])
        count += 2

    # split limiter into (R,Z) pairs
    count = 0
    rlim = []
    zlim = []
    while count < len(limit_temp) - 1:
        rlim.append(limit_temp[count])
        zlim.append(limit_temp[count + 1])
        count += 2

    g = dict(nw=nw, nh=nh, rdim=rdim, zdim=zdim, rcentr=rcentr, rleft=rleft,
        zmid=zmid, rmaxis=rmaxis, zmaxis=zmaxis, simag=simag, sibry=sibry,
        current=current, fpol=np.array(fpol), ffprime=np.array(ffprime),
        pprime=np.array(pprime), psirz=np.array(psirz), qpsi=np.array(qpsi),
        nbbbs=nbbbs, bcentr=bcentr, pres=np.array(pres), limitr=limitr,
        rbdry=np.array(rbdry), zbdry=np.array(zbdry), rlim=np.array(rlim),
        zlim=np.array(zlim))

    return g




##Should be mpsi
## fpsi (normal), mpsi (normalized), rpsi (rhopol, square root)
## ATTENTION: Standarddize names
def get_mpsi(gfile):
    g = loadg(gfile)
    mpsi = (g['psirz'] - g['simag']) / (g['sibry'] - g['simag'])
    dr = g['rdim'] / (g['nw'] - 1)
    dz = g['zdim'] / (g['nh'] - 1)
    r = g['rleft'] + np.arange(0,g['nw'])*dr
    z = g['zmid'] - 0.5*g['zdim'] + np.arange(0,g['nh'])*dz #zmid and zdim
    return r, z, mpsi

## ATTENTION: Standarddize names
def mpsi(gfile):
    """
    Returns a bicubic spline interpolation, so that mpsi(R,z) is correct.

    Notes
    -----
    The reason behind the transpose of mpsi is because everything is a mess.
    In this way, we can evaluate mpsi as expected mpsi(R,z),
    otherwise, it is wrong.
    """
    data = get_mpsi(gfile)
    try:
        return RGI((data[0], data[1]), data[2].T, method = 'cubic')
    except:
        slog.warning("RGI of older scipy does not accept cubic splines.")
        slog.warning("Using deprecated interp2d instead.")
        return interp2d(data[0],data[1],data[2], kind='cubic')

