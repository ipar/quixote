
import os
import re
import sys
import gzip
import itertools
import subprocess
import numpy as np
from functools import wraps
import collections
from collections import OrderedDict, deque, defaultdict
from collections.abc import Callable
import logging
import copy
import yaml



from .core import *
from .decorators import *

slog = logging.getLogger('quixote')








def cellface(c1, c2):
    """
    """
    interface = np.zeros((2,2))
    k=0
    for i in range(c2.shape[0]):
        try:
            ind = np.where((np.isclose(c1, c2[i]).all(axis=1)))[0][0]
            interface[k] = c1[ind]
            k+=1
        except:
            pass
    return interface






class LinAlg2d(object):
    """A simple class to handle lines and intersections of lines in 2D.
    I am almost sure, there must be something like this already, but I
    couldn't find anything. Maybe this can be replaced (and just stay
    as an alias or so)."""

    def __init__(self):
        pass

    class Line(object):
        """A line created from two points."""
        def __init__(self,start,end):
            self.start = np.array(start)
            self.end   = np.array(end)
            self.direction   = self.end - self.start

    class LineDir(Line):
        """A line created from one point and a direction."""
        def __init__(self, start, direction):
            self.start = np.array(start)
            self.direction   = np.array(direction)
            self.end   = self.start + self.direction

    @staticmethod
    def find_intersection(line1,line2):
        """Returns the intersection point of two lines line1 and line2"""
        Ax = line1.start[0]
        Ay = line1.start[1]
        ax = line1.direction[0]
        ay = line1.dir[1]
        Bx = line2.start[0]
        By = line2.start[1]
        bx = line2.direction[0]
        by = line2.direction[1]
        if ax*by - ay*bx == 0: # Parallel lines, no intersection!
            return None
        t  = ( (Bx-Ax)*ay - (By-Ay)*ax ) / ( ax*by - ay*bx )
        return np.array([Bx+t*bx,By+t*by])

    @staticmethod
    def calculate_distance(point1,point2):
        p1 = np.array(point1)
        p2 = np.array(point2)
        return np.linalg.norm(p2-p1)

    @staticmethod
    def points_on_same_side_of_line(point1,point2,line):
        """Returns True if point1 and point2 are on the same side of line"""
        # Make a line out of point1 and point2:
        pline = LinAlg2d.Line(point1,point2)
        # Find the intersection:
        # (or how much pline needs to be extended to the intersection)
        Ax = line.start[0]
        Ay = line.start[1]
        ax = line.direction[0]
        ay = line.direction[1]
        Bx = pline.start[0]
        By = pline.start[1]
        bx = pline.direction[0]
        by = pline.direction[1]
        t  = ( (Bx-Ax)*ay - (By-Ay)*ax ) / ( ax*by - ay*bx )

        if 0.000001 < t < 0.999999: # (If the point is on the line, numerical
            return False          # imprecision might result in the wrong
        else:                     # outcome when using 0 < t < 1 ...)
            return True

    @staticmethod
    def point_inside_cell(cell,point):
        """Returns True if the point is inside of the cell"""
        # cell must be a run.grid[nx,ny] array

        # Cell Center:
        x = np.average(cell[:,0])
        y = np.average(cell[:,1])
        cc = np.array([x,y])

        # Cell Faces:
        cf = []
        for i in range(cell.shape[0]):
            if i == 0:
                face = LinAlg2d.Line(cell[-1],cell[i])
            else:
                face = LinAlg2d.Line(cell[i-1],cell[i])
            cf.append(face)
        for face in cf:
            if not LinAlg2d.points_on_same_side_of_line(cc,point,face):
                return False
        return True

    @staticmethod
    def distance_cylindrical_coordinates(start,end):
        """Calculate the distance between two
        points given in cylindrical coordinates."""
        # Convert to cartesian coordinates:
        x = (end  [0]*np.cos(end  [2]*np.pi/180.0)-
             start[0]*np.cos(start[2]*np.pi/180.0))
        y = (end  [0]*np.sin(end  [2]*np.pi/180.0)-
             start[0]*np.sin(start[2]*np.pi/180.0))
        z =  end  [1]   -    start[1]
        return np.linalg.norm(np.array([x,y,z]))



