"""
Adaptation of SOLPS utils
"""

import numpy as np

def intcell(mother, var):
    """
    """
    ncv = mother.ncv
    nfc = mother.nfc
    icvfc = mother.icvfc
    edims= var.shape[1:]
    center = np.full(extend(ncv,edims), np.nan)
    for icv in range(ncv):
        pass

def intface(mother, var):
    """
    utility/intcell.F
    In SOLPS, it seems that only (ncv) or (ncv, ns)
    but I think I can just do this.
    """
    nfc = mother.nfc
    ncv = mother.ncv
    ifccv = mother.ifccv
    edims= var.shape[1:]
    face = np.full(extend(nfc,edims), np.nan)
    for ifc in range(nfc):
        pass

def intvertex(mother, var):
    """
    """
    ncv = mother.ncv
    nvx = mother.nvx
    icvvx = mother.icvvx
    edims= var.shape[1:]
    center = np.full(extend(ncv,edims), np.nan)
    for icv in range(ncv):
        pass
