"""
Description:
    Provides some tools used in plotting routines.

    Also allows setting the dpi value and the transparency for '.png'
    images via the following environment variables:
     - PYTHON_PLOT_DPI                  (Default: 72)
     - PYTHON_PLOT_TRANSP_BCKG          (Default: 1)
    (Will be set when this module is loaded.)
"""

# Common modules
import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle


# Quixote modules
from .utils import priority, update, create_log
from .utils import get, extend


alog = create_log("tools.plots")

##ATTENTION 2023: Is this good???
## =============================================================================
#
## Read Environment variables:
#
#PYTHON_PLOT_DPI         = os.environ.get('PYTHON_PLOT_DPI',300)
#PYTHON_PLOT_TRANSP_BCKG = os.environ.get('PYTHON_PLOT_TRANSP_BCKG',True)
#PYTHON_PLOT_TRANSP_BCKG = bool(int(PYTHON_PLOT_TRANSP_BCKG))
#
### Globally set '.png' dpi value:
##matplotlib.rcParams['savefig.dpi'] = PYTHON_PLOT_DPI
#
## =============================================================================

def savefig(saveas,default='pdf',transp_bckg=None):
    if saveas is None:          return
    if saveas is False:         return
    if type(saveas) is not str: return
    if (not saveas.endswith('.png') and 
        not saveas.endswith('.pdf') and
        not saveas.endswith('.eps')):
        saveas += ('.' + default).replace('..','.')
    if transp_bckg is None:
        transp_bckg = PYTHON_PLOT_TRANSP_BCKG
    plt.savefig(saveas,transparent=transp_bckg)
    print("Created file: '" + saveas + "'")
    return saveas

def convert_figsize(figsize):
    """Converts the figure size to inches if it is given
    in mm or cm (which can be specified as a third index
    in the figzise tuple)."""
    if len(figsize) == 3:
        if figsize[2] == 'mm':
            f = 0.0393701
            figsize = (f*figsize[0],f*figsize[1])
        elif figsize[2] == 'cm':
            f = 0.393701
            figsize = (f*figsize[0],f*figsize[1])
        elif figsize[2] == 'inch' or figsize[2] == 'in':
            figsize = (figsize[0],figsize[1])
    return figsize


def shifted_color_map(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    """
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower ofset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax/(vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highets point in the colormap's range.
          Defaults to 1.0 (no upper ofset). Should be between
          `midpoint` and 1.0.

    Notes
    -----
    From stackoverflow:
    https://stackoverflow.com/questions/7404116/
    defining-the-midpoint-of-a-colormap-in-matplotlib
    """
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False),
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap




def get_datalim(canvas, tight=False):
    """ Similar to the internal function of autoscale_view()
    It works for just ONE axis i.e ONE canvas and the lines in that canvas.
    axes/_base.py(1899)


    import matplotlib.transforms as mtransforms
    # X-axis
    dl = canvas.dataLim
    finite_dl = [d for d in dl if np.isfinite(d).all()]
    bb = mtransforms.BboxBase.union(dl)
    x0, x1 = bb.intervalx
    xlocator = canvas.xaxis.get_major_locator()
    try:
        x0,x1 = xlocator.nonsingular(x0,x1)
    except AttributeError:
        x0,x1 = mtransforms.nonsingular(x0,x1,increasing=False, expander=0.05)

    if not _tight:
        x0,x1 xlocator,view_limits(x0,x1) #Modified here
    self.set_xbound(x0,x1)

    self.set_xbound seems to add a little bit of padding around.
    """
    import matplotlib.transforms as mtransforms

    # X- axis
    xshared = canvas._shared_x_axes.get_siblings(canvas)
    dl = [ax.dataLim for ax in xshared]
    finite_dl = [d for d in dl if np.isfinite(d).all()]
    if len(finite_dl):
        dl = finite_dl
    bb = mtransforms.BboxBase.union(dl)
    x0, x1 = bb.intervalx

    # Y- axis
    yshared = canvas._shared_y_axes.get_siblings(canvas)
    dl = [ax.dataLim for ax in yshared]
    finite_dl = [d for d in dl if np.isfinite(d).all()]
    if len(finite_dl):
        dl = finite_dl
    bb = mtransforms.BboxBase.union(dl)
    y0, y1 = bb.intervaly

    return (x0,x1), (y0,y1)























def initialize_canvas(canvas=None, **kwargs):
    """ Type-agnostic creation and/or processing of canvas.

    Parameters
    ----------

    canvas: matplotlib canvas, optional.
        If given, then processing will be applied to it.
        If not given, then a new canvas will be created and processed.
        Default: None.

    figsize: tuple or list, optional.
        If a new canvas is to be created, size of the figure in which the
        canvas will be contained.
        Default: matplotlib default.

    title: str, optional
        Title of the canvas. Default: None.

    figtitle: str, optional
        Title of the figure of the canvas. Default: None.

    xlabel: str, optional
        Label of the x-axis. Default: None.

    ylabel: str, optional
        Label of the y-axis. Default: None.

    adjust: dict or list, optional
        Suplot adjustments to padding (left, top, right, bottom).
        Default: None.



    Returns
    -------
    fig: matplotlib figure.
    canvas: matplotlib canvas.


    Notes
    -----
    Add possibility of [xy]ticks and [xy]tickslabels.
    """

    if not canvas: #Then create the canvas
        fig, canvas = create_canvas(**kwargs)
    else: #If canvas is given, just get the associated figure
        fig = canvas.get_figure()

    kw = priority('kw', kwargs, {})
    no_processing = priority(['no_processing', 'untouch'],kwargs,kw,False)
    if no_processing == False:
        fig, canvas = process_canvas(canvas=canvas,**kwargs)

    return fig, canvas



def create_canvas(**kwargs):
    """ Create a canvas after filtering the given kwargs.
    """
    kw = priority('kw', kwargs, {})
    figsize = priority(['figsize', 'fig_size'], kwargs, kw, None)

    creation_kw = {}
    if figsize:
        creation_kw['figsize'] = figsize

    return plt.subplots(**creation_kw)


def process_canvas(canvas, **kwargs):
    """ Process a canvas after filtering the given kwargs.
    """
    fig = canvas.get_figure()

    kw = priority('kw', kwargs, {})

    figsize = priority(['figsize', 'fig_size'], kwargs, kw, None)

    title = priority('title', kwargs, kw, None)
    fig_title = priority(['figtitle', 'fig_title'], kwargs, kw, None)

    xlabel = priority('xlabel', kwargs, kw, None)
    ylabel = priority('ylabel', kwargs, kw, None)

    xlim = priority('xlim', kwargs, kw, None)
    ylim = priority('ylim', kwargs, kw, None)

    aspect = priority('aspect', kwargs, kw, None)

    xticklabels = priority('xticklabels', kwargs, kw, 'default')
    yticklabels = priority('ylicklabels', kwargs, kw, 'default')

    xticks = priority('xtick', kwargs, kw, 'default')
    yticks = priority('ylick', kwargs, kw, 'default')
    xticks_m = priority('xtick_m', kwargs, kw, 'default')
    yticks_m = priority('ylick_m', kwargs, kw, 'default')

    adjust = priority('adjust', kwargs, kw, None)

    #Set titles
    if title:
        canvas.title(title)
    if fig_title:
        fig.title(fig_title)

    # Set labels
    if xlabel:
        canvas.set_xlabel(xlabel)
    if ylabel:
        canvas.set_ylabel(ylabel)

    # Set aspect ratio and limits
    if xlim:
        canvas.set_xlim(xlim)
    if ylim:
        canvas.set_ylim(ylim)

    # Set aspect ratio and limits
    if xlim is not None or ylim is not None:
        if aspect:
            canvas.axes.set_aspect(aspect) # Allow [xy]lim
        if ylim is not None:
            canvas.set_ylim(ylim)
        if xlim is not None:
            canvas.set_xlim(xlim)
    elif aspect:
        canvas.axis(aspect)

    ## These set the labels on the ticks, not the ticks themselves.
    if xticklabels is not None and xticklabels != 'default':
        canvas.axes.set_xticklabels(xticklabels)
    elif xticklabels is None:
        canvas.axes.set_xticklabels([])
    if yticklabels is not None and yticklabels != 'default':
        canvas.axes.set_yticklabels(yticklabels)
    elif yticklabels is None:
        canvas.axes.set_yticklabels([])

    # Set the major ticks
    if xticks is not None and xticks != 'default':
        canvas.axes.set_xticks(xticks)
    elif xticks is None:
        canvas.axes.set_xticks([])
    if yticks is not None and yticks != 'default':
        canvas.axes.set_yticks(yticks)
    elif yticks is None:
        canvas.axes.set_yticks([])

    # Set the minor ticks
    if xticks_m is not None and xticks_m != 'default':
        canvas.axes.set_xticks(xticks_m, minor=True)
    elif xticks is None:
        canvas.axes.set_xticks([], minor=True)
    if yticks_m is not None and yticks_m != 'default':
        canvas.axes.set_yticks(yticks_m, minor=True)
    elif yticks is None:
        canvas.axes.set_yticks([], minor=True)



    # Adjusts subplots (mostly, white spacing around axis)
    if   adjust and isinstance(adjust, dict):
        fig.subplots_adjust(**adjust)
    elif adjust and isinstance(adjust, list):
        fig.subplots_adjust(*adjust)

    return fig, canvas





## ATTENTION: Maybe accept via config different dfault paths
## ATTENTION: Improve logic and prepare to be used as context manager:
## Allow for path to style to be passed directly.
def styles(name=None):
    """
    """
    styles_path = os.path.join(module_path(), 'mplstyles')
    if not name:
        return [f[:-9] for f in os.listdir(styles_path)
                if os.path.isfile(os.path.join(styles_path, f))]
    else:
        try:
            if name[-9:] == '.mplstyle':
                return os.path.join(styles_path,name)
            else:
                return os.path.join(styles_path,name+'.mplstyle')
        except:
            return None





## ATTENTION: Allow to pass things in different formas correctly
##ATTENTION: Allow for separation by white space!
## Allow just adding lines with kwargs
def legend(**kwargs):
    """
        if done with set_style, pairs is generated automatically
    if not given by the user.


    order: list

        Order in which the kwargs will be shown
    """
    order = priority('order', kwargs, ['color', 'ls', 'marker', 'lw'])

    lns = priority(['lines', 'lns'], kwargs, [])

    lines = []
    names = []

    for pretty in order:
        if pretty in kwargs:
            ## Lines with defaults other than set
            if isinstance(kwargs[pretty], dict):
                tmp = kwargs[pretty]
                for name, uniq in tmp.items():
                    lines.append(plt.Line2D([0], [0],
                        **update({pretty:uniq})))
                    names.append(name)

            ## Lines with changes to defaults
            elif isinstance(kwargs[pretty], list):
                tmp = kwargs[pretty]
                for name, uniq in tmp[0].items():
                    lines.append(plt.Line2D([0], [0],
                        **update(tmp[1],{pretty:uniq})))
                    names.append(name)

    #for line in lns:
    #    for k, v in line.items():
    #        if k.lower() == 'label' or k.lower() == 'name':
    #            names.append(v)
    #        else:
    #            lines.append(plt.Line2D([0], [0],
    #                **update({k:v})))

    for line in lns:
        if 'label' in line:
                names.append(line['label'])
                del line['label']
        elif 'name' in line:
                names.append(line['name'])
                del line['name']

        lines.append(plt.Line2D([0], [0], **update(line)))


    return [lines, names]







#def errorplot(xobj, yobj, **kwargs):
#    """
#    xobj: np.array 
#
#
#    ptype: str
#        Can be either 'errorbars', 'area'/'fill', or 'boxes'.
#    """
#    canvas = priority(['canvas', 'ax'], kwargs, None)
#    ptype = priority(['plot','ptype'], kwargs, 'errorbars')
#    erroraxis= priority(['ea','erroraxis','axis'], kwargs,0)
#
#    pretty=  {}
#    prettyline=  {}
#    prettyerror=  {}
#    prettyarea={}
#    prettybox={}
#    color = priority(['color','colour','c'], kwargs, None)
#    if color is not None:
#        pretty['color'] = color
#    prettyerror['ecolor'] = priority(['ecolor','ecolour','ec'], kwargs, None)
#    prettyline['lw'] = priority(['lw','linewidth'],
#            kwargs, plt.rcParams['lines.linewidth'])
#    prettyerror['elinewidth'] = priority(['elw','elinewidth'], kwargs, None)
#    prettyline['ls'] = priority(['ls','linestyle'], kwargs, '-')
#    prettyerror['capsize'] = priority('capsize', kwargs, 0.0)
#    #prettyerror['capthick'] = priority('capthick', kwargs, 0.0)
#
#
#    prettyline['marker'] = priority('marker', kwargs, 
#            plt.rcParams['lines.marker'])
#    prettyline['ms'] = priority(['ms','markersize'], kwargs, 
#            plt.rcParams['lines.markersize'])
#
#
#    pretty['alpha'] = priority(['alpha'], kwargs, 1.0)
#    prettyarea['alpha'] = priority(['alpha_area'], kwargs, 0.3)
#
#
#
#    prettybox['box_alpha'] = priority(['alpha_box', 'box_alpha'], kwargs, 0.3)
#
#    prettybox['box_color'] = priority(['color_box', 'box_color'], kwargs, None)
#    if ptype=='boxes' and not prettybox['box_color']:
#        prettybox['box_color'] = 'auto'
#    if prettybox['box_color'] == 'auto':
#        prettybox['box_color'] = pretty['color']
#
#    prettybox['box_edgecolor'] = priority(['edgecolor_box', 'box_edgecolor'], kwargs, 'auto')
#    if prettybox['box_edgecolor'] == 'auto':
#        prettybox['box_edgecolor'] = prettybox['box_color']
#
#
#    if canvas is None:
#        _, canvas = initialize_canvas(**kwargs)
#
#
#    ## Rethink this logic because it is stupid
#    yobj = yobj.squeeze()
#    if yobj.ndim > 2:
#        alog.exception(
#        "yobj ndim must be >2, and at least one must have len == 3 or 2.")
#    elif yobj.ndim == 2:
#        if erroraxis == 0:
#            y = yobj
#        elif erroraxis ==1:
#            y = yobj.T
#        ## Rethink this logic because it is stupid
#        ## Define everything correctly, as it should be, in the documentation
#        if y.shape[erroraxis] ==3:
#            yerr = np.abs(y[1:] - y[0])
#        elif y.shape[erroraxis] ==2:
#            yerr = y[1]
#        y = y[0]
#    elif yobj.ndim == 1:
#        y = yobj
#        yerr = None
#
#
#    xobj = xobj.squeeze()
#    if xobj.ndim > 2:
#        alog.exception(
#        "xobj ndim must be >2, and at least one must have len == 3 or 2.")
#    elif xobj.ndim == 2:
#        if erroraxis == 0:
#            x = xobj
#        elif erroraxis ==1:
#            x = xobj.T
#        if x.shape[erroraxis] ==3:
#            xerr = np.abs(x[1:] - x[0])
#        elif x.shape[erroraxis] ==2:
#            xerr = x[1]
#        x = x[0]
#    elif xobj.ndim == 1:
#        x = xobj
#        xerr = None
#
#
#    def create_boxes(x, y, xerr, yerr):
#        errorboxes = []
#        if xerr.ndim == 1:
#            xerr = np.vstack([xerr, xerr])
#        if yerr.ndim == 1:
#            yerr = np.vstack([yerr, yerr])
#        for x1, y1, xe1, ye1 in zip(x,y,xerr.T, yerr.T):
#            errorboxes.append(
#            Rectangle((x1-xe1[0],y1-ye1[0]), xe1.sum(), ye1.sum()))
#        return errorboxes
#
#
#
#    artists = []
#    if ptype=='error' or ptype=='errorbars' or ptype=='boxes':
#        if ptype=='boxes' and prettybox['box_color']:
#            errorboxes = create_boxes(x,y,xerr,yerr)
#            pc = PatchCollection(errorboxes, facecolor=prettybox['box_color'],
#                alpha = prettybox['box_alpha'], edgecolor=prettybox['box_edgecolor'])
#            artists.append(pc)
#            canvas.add_collection(pc)
#        prettify= update(pretty, update(prettyline, prettyerror))
#        artists.append(canvas.errorbar(x, y, xerr=xerr, yerr=yerr, **prettify))
#
#    elif ptype == 'area' or ptype == 'fill':
#        artists.append(canvas.fill_between(x, y-yerr[0], y+yerr[1],
#            **update(pretty, prettyarea)))
#        artists.append(canvas.plot(x, y, **update(pretty, prettyline)))
#
#
#    return canvas, artists










def errorplot(xobj, yobj, **kwargs):
    """
    xobj: np.array 


    ptype: str
        Can be either 'errorbars', 'area'/'fill', or 'boxes'.
    """
    canvas = priority(['canvas', 'ax'], kwargs, None)
    ptype = priority(['plot','ptype'], kwargs, 'errorbars')
    erroraxis= priority(['ea','erroraxis','axis'], kwargs,0)

    pretty=  {}
    prettyline=  {}
    prettyerror=  {}
    prettyarea={}
    prettybox={}
    color = priority(['color','colour','c'], kwargs, None)
    if color is not None:
        pretty['color'] = color
    prettyerror['ecolor'] = priority(['ecolor','ecolour','ec'], kwargs, None)
    prettyline['lw'] = priority(['lw','linewidth'],
            kwargs, plt.rcParams['lines.linewidth'])
    prettyerror['elinewidth'] = priority(['elw','elinewidth'], kwargs, None)
    prettyline['ls'] = priority(['ls','linestyle'], kwargs, '-')
    prettyerror['capsize'] = priority('capsize', kwargs, 0.0)
    #prettyerror['capthick'] = priority('capthick', kwargs, 0.0)


    prettyline['marker'] = priority('marker', kwargs, 
            plt.rcParams['lines.marker'])
    prettyline['ms'] = priority(['ms','markersize'], kwargs, 
            plt.rcParams['lines.markersize'])


    pretty['alpha'] = priority(['alpha'], kwargs, 1.0)
    prettyarea['alpha'] = priority(['alpha_area'], kwargs, 0.3)



    prettybox['box_alpha'] = priority(['alpha_box', 'box_alpha'], kwargs, 0.3)

    prettybox['box_color'] = priority(['color_box', 'box_color'], kwargs, None)
    if ptype=='boxes' and not prettybox['box_color']:
        prettybox['box_color'] = 'auto'
    if prettybox['box_color'] == 'auto':
        prettybox['box_color'] = pretty['color']

    prettybox['box_edgecolor'] = priority(['edgecolor_box', 'box_edgecolor'], kwargs, 'auto')
    if prettybox['box_edgecolor'] == 'auto':
        prettybox['box_edgecolor'] = prettybox['box_color']


    if canvas is None:
        _, canvas = initialize_canvas(**kwargs)


    ## Rethink this logic because it is stupid
    yobj = yobj.squeeze()
    if yobj.ndim > 2:
        alog.exception(
        "yobj ndim must be >2, and at least one must have len == 3 or 2.")
    elif yobj.ndim == 2:
        if erroraxis == 0:
            y = yobj
        elif erroraxis ==1:
            y = yobj.T
        ## Rethink this logic because it is stupid
        ## Define everything correctly, as it should be, in the documentation
        if y.shape[erroraxis] ==3:
            yerr = np.abs(y[1:] - y[0])
        elif y.shape[erroraxis] ==2:
            yerr = y[1]
        y = y[0]
    elif yobj.ndim == 1:
        y = yobj
        yerr = None


    xobj = xobj.squeeze()
    if xobj.ndim > 2:
        alog.exception(
        "xobj ndim must be >2, and at least one must have len == 3 or 2.")
    elif xobj.ndim == 2:
        if erroraxis == 0:
            x = xobj
        elif erroraxis ==1:
            x = xobj.T
        if x.shape[erroraxis] ==3:
            xerr = np.abs(x[1:] - x[0])
        elif x.shape[erroraxis] ==2:
            xerr = x[1]
        x = x[0]
    elif xobj.ndim == 1:
        x = xobj
        xerr = None


    def create_boxes(x, y, xerr, yerr):
        errorboxes = []
        if xerr.ndim == 1:
            xerr = np.vstack([xerr, xerr])
        if yerr.ndim == 1:
            yerr = np.vstack([yerr, yerr])
        for x1, y1, xe1, ye1 in zip(x,y,xerr.T, yerr.T):
            errorboxes.append(
            Rectangle((x1-xe1[0],y1-ye1[0]), xe1.sum(), ye1.sum()))
        return errorboxes



    artists = []
    if ptype=='error' or ptype=='errorbars' or ptype=='boxes':
        if ptype=='boxes' and prettybox['box_color']:
            errorboxes = create_boxes(x,y,xerr,yerr)
            pc = PatchCollection(errorboxes, facecolor=prettybox['box_color'],
                alpha = prettybox['box_alpha'], edgecolor=prettybox['box_edgecolor'])
            artists.append(pc)
            canvas.add_collection(pc)
        prettify= update(pretty, update(prettyline, prettyerror))
        artists.append(canvas.errorbar(x, y, xerr=xerr, yerr=yerr, **prettify))

    elif ptype == 'area' or ptype == 'fill':
        artists.append(canvas.fill_between(x, y-yerr[0], y+yerr[1],
            **update(pretty, prettyarea)))
        artists.append(canvas.plot(x, y, **update(pretty, prettyline)))


    return canvas, artists














def cells_convex_hull(mother, cells):
   """ Start with one cell and add all the faces.
   Then remove common faces with the next cells if they are in common.
   For structured, this is a matter of checking whether they are neighbours of some kind.
   For unstructured, actually save ifc, much easier.


   Faces with r, z, in physical space (I think):

      3---y1----1
      |         |
      x1       x0
      |         |
      2---y0----0

   """


   if mother.grid_type=='structured':
      ifx = {}
      ify = {}
      for ix, iy in cells:
         r = mother.r[ix,iy]
         z = mother.z[ix,iy]
         x0 = ix + iy*(mother.nx-1)
         x1 = ix + (iy+1)*(mother.nx-1)
         y0 = iy + ix*(mother.ny-1)
         y1 = iy + (ix+1)*(mother.ny-1)

         if x0 in ifx:
            del ifx[x0]
         else:
            ifx[x0] = np.vstack([[r[0], z[0]], [r[1], z[1]]]) 

         if x1 in ifx:
            del ifx[x1]
         else:
            ifx[x1] = np.vstack([[r[3], z[3]], [r[2], z[2]]]) 

         if y0 in ify:
            del ify[y0]
         else:
            ify[y0] = np.vstack([[r[2], z[2]], [r[0], z[0]]]) 

         if y1 in ify:
            del ify[y1]
         else:
            ify[y1] = np.vstack([[r[1], z[1]], [r[3], z[3]]]) 

      faces = []
      for _, fx in ifx.items():
         faces.append(fx)
      for _, fy in ify.items():
         faces.append(fy)
   elif mother.grid_type =='unstructured':
      pass #Do the same, but only with one list using icvfc

   return np.array(faces)








def cumsum_quantile_cells(mother, var, quantile=0.50):
    if isinstance(var, str):
        var = get(mother,var)

    fvar = var.flatten()
    xy = np.full((mother.nx, mother.ny), np.nan, dtype='object')
    for ix in range(mother.nx):
       for iy in range(mother.ny):
          xy[ix,iy] = (ix,iy)
    
    xy = xy.flatten()
    
    idens = np.argsort(fvar)
    density = fvar[idens]
    sxy = xy[idens]
    cdf = (density/density.sum()).cumsum()
    exclude_quantile = 1 - quantile
    idensity = cdf.searchsorted(exclude_quantile)
    return sxy[idensity:]








def levels_quantile_cells(mother, var, level, maxlevel=None):
    if isinstance(var, str):
        var = get(mother,var)

    fvar = var.flatten()
    xy = np.full((mother.nx, mother.ny), np.nan, dtype='object')
    for ix in range(mother.nx):
       for iy in range(mother.ny):
          xy[ix,iy] = (ix,iy)
    
    xy = xy.flatten()
    
    idens = np.argsort(fvar)
    ordered_var = fvar[idens]
    sxy = xy[idens]
    idensity = ordered_var.searchsorted(level)
    if not maxlevel:
        return sxy[idensity:]
    else:
        idensity2 = ordered_var.searchsort(maxlevel)
        return sxy[idensity:idensity2+1]







#def highlight(canvas, mother, cells, **kwargs):
#    faces = cells_convex_hull(mother, cells)
#    for face in faces:
#       canvas.plot(face[:,0], face[:,1], **kwargs)
#    return

def highlight(canvas, mother, cells, zreversed=False, **kwargs):
    if zreversed:
        zf = -1.0
    else:
        zf = 1.0
    faces = cells_convex_hull(mother, cells)
    for face in faces:
       canvas.plot(face[:,0], zf*face[:,1], **kwargs)
    return






def cumsum_quantiles(canvas, mother, var, zreversed=False, **kwargs):
        
    if isinstance(var, str):
        var = get(mother,var)

    quantiles = extend(priority(['quantiles','quantile','q'], kwargs, []))
    if quantiles == []:
        alog.exception("'quantile', 'quantiles', or 'q' required")
        return

    nq = len(quantiles)

    colors = priority(['color', 'colors'], kwargs, 'r')
    if isinstance(colors, str):
        colors= extend(colors)*nq

    lss = priority(['ls', 'lss'], kwargs, '-')
    if isinstance(lss, str):
        lss= extend(lss)*nq

    lws = priority(['lw', 'lws'], kwargs, 1.0)
    if isinstance(lws, float) or isinstance(lws, int):
        lws= extend(lws)*nq

    ## cmap takes priority over color
    cmap = priority(['cmap'], kwargs, None)
    if cmap is not None:
        if isinstance(cmap, str):
            cm = matplotlib.cm.get_cmap(cmap)
        else:
            cm = cmap
        sampling = np.linspace(0,1,nq+2)[1:-1] #Alternatively, nq so take the extremes
        colors = []
        for samp in sampling:
            colors.append(cm(samp))

    for quantile, color, ls, lw in zip(quantiles, colors, lss, lws):
        cells = cumsum_quantile_cells(mother, var, quantile)
        #highlight(canvas, mother, cells, color=color, ls=ls, lw=lw)
        highlight(canvas, mother, cells,
            zreversed=zreversed, color=color, ls=ls, lw=lw)

    return


def _inputs_to_levels(inputs, var):
    tmp = []
    inputs = extend(inputs)
    for inp in inputs:
        if isinstance(inp, float) or isinstance(inp, int):
            tmp.append(inp)
        elif isinstance(inp, str):
            dummy = inp.split('*')
            if len(dummy) ==1:
                tmp.append(inp)
            elif dummy[1].lower() == 'max':
                tmp.append(float(dummy[0])*np.max(var))
            elif dummy[1].lower() == 'min':
                tmp.append(float(dummy[0])*np.min(var))
            else:
                alog.exception("Instruction '{}' not parseable".format(inp))
    return tmp





def contours(canvas, mother, var, zreversed=False, **kwargs):
        
    if isinstance(var, str):
        var = get(mother,var)


    levels = priority(['levels', 'level', 'lvls', 'lvl'], kwargs, [])
    levels = _inputs_to_levels(levels, var)

    ofmax = extend(priority(['ofmax', 'of_max'], kwargs, []))
    ofmax = [ x*np.max(var) for x in ofmax]
    ofmin = extend(priority(['ofmin', 'of_min'], kwargs, []))
    ofmin = [ x*np.min(var) for x in ofmin]
    levels = list(dict.fromkeys(extend(levels, ofmax, ofmin)))
    if levels == []:
        alog.exception("Levels must be provided")
        return
    nl = len(levels)

    maxlim = priority(['maxlevel', 'limlevel', 'maxlim'], kwargs, None)
    if maxlim is not None:
        maxlim = _inputs_to_levels(extend(maxlim), var)



    colors = priority(['color', 'colors'], kwargs, 'r')
    if isinstance(colors, str):
        colors= extend(colors)*nl
    elif isinstance(colors, tuple):
        colors=[colors]*nl

    lss = priority(['ls', 'lss'], kwargs, '-')
    if isinstance(lss, str):
        lss= extend(lss)*nl

    lws = priority(['lw', 'lws'], kwargs, 1.0)
    if isinstance(lws, float) or isinstance(lws, int):
        lws= extend(lws)*nl

    ## cmap takes priority over color
    cmap = priority(['cmap'], kwargs, None)
    if cmap is not None:
        if isinstance(cmap, str):
            cm = matplotlib.cm.get_cmap(cmap)
        else:
            cm = cmap
        sampling = np.linspace(0,1,nl+2)[1:-1]
        colors = []
        for samp in sampling:
            colors.append(cm(samp))

    for level, color, ls, lw in zip(levels, colors, lss, lws):
        cells = levels_quantile_cells(mother, var, level, maxlevel=maxlim)
        #highlight(canvas, mother, cells, color=color, ls=ls, lw=lw)
        highlight(canvas, mother, cells,
            zreversed=zreversed, color=color, ls=ls, lw=lw)

    return




def separatrix(canvas, mother, **kwargs):

    wsep = priority(['wsep', 'which_sep', 'which', 'sep', 'seps'], kwargs, 0)
    wsep = extend(wsep)
    
    colors = priority(['color', 'colors'], kwargs, 'r')
    colors= extend(colors)
    if len(colors) < len(wsep):
        colors *= len(wsep)

    lws = priority(['lw', 'lws', 'linewidth', 'linewidths'], kwargs,
        plt.rcParams['lines.linewidth'])
    lws = extend(lws)
    if len(lws) < len(wsep):
        lws *= len(wsep)

    lss = priority(['ls', 'lss', 'linestyle', 'linestyles'], kwargs, '-')
    lss = extend(lss)
    if len(lss) < len(wsep):
        lss *= len(wsep)


    if mother.grid_type == 'structured':
        sep_index = [mother.isep]
        try:
            sep_index.append(mother.isep2)
        except:
            pass
        #bottomfaces = [3,2] ## This is top face I think
        bottomfaces = [0,1] ## I think??
        for isepi, color, lw, ls in zip(sep_index, colors, lws, lss):
            for ix in range(mother.nx):
                canvas.plot(mother.r[ix, isepi, bottomfaces], mother.z[ix, isepi, bottomfaces],
                    color = color, lw=lw, ls=ls)

    elif mother.grid_type == 'unstructured':
        pass

    return canvas


