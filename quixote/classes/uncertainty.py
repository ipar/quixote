import numpy as np

from .family import Family

from ..tools import parse_dataframe
from ..tools import extend, update, get
from ..tools import priority
from ..tools import initialize_canvas

#Only temporal
import matplotlib.pyplot as plt


## ----------------------------------------------------------------------------
class Uncertainty(Family):
    def __init__(self, *args, **kwargs):
        """
        """
        super().__init__(*args, **kwargs)

        self.mdb = self.db.dropna(subset='main').reset_index(drop=True)

        uniques = self.db['group'].unique()
        
        #import ipdb; ipdb.set_trace()
        #self.mdb['entries'] = [len(
        #    parse_dataframe(self.db, {'group': tag})) for tag in uniques]
        tmp = []
        for tag in uniques:
            if isinstance(tag, (int, float)) and np.isnan(tag):
                continue
            tmp.append(len(parse_dataframe(self.db, {'group': tag})))
        self.mdb['entries'] = tmp


    def __getitem__(self, *args):
        if not args:
            return None
        elif not self.built:
            self.log.error("DB not built yet")
        else:
            args = extend(*args)
            if isinstance(args[0],str):
                return self.mdb[args].squeeze()
            else:
                return self.mdb.iloc[args].squeeze()



    def show(self, cols=None, **kwargs):
        db = priority(['db','database'], kwargs, 'mdb')
        always = priority(['always', 'show_always', 'always_show'], kwargs, ['group', 'entries'])
        print(self._get_db_view(extra_cols=cols, db=db, show_always=always, **kwargs).to_string())
        return

    ## ATTENTION: Maybe only accept kwargs to avoid problems
    def sample(self, function, main='average',
            args=[], kwargs={}, use_nan=True,
            squeeze=True):
        """
        In the future, add a way to sample cases in different cores/threads.
        """
        uniques = self.db['group'].unique()
        samples = super().sample(function,
            args=args, kwargs=kwargs, use_nan=use_nan)
        dims = extend(len(uniques), 3, samples.shape[1:])
        uncertainty = np.full(dims, np.nan)
        for i, tag in enumerate(uniques):
            inds = parse_dataframe(self.db, {'group': tag}).index
            mind = parse_dataframe(self.db, {'group': tag, 'main': tag}).index
            if len(mind)==0:
                pass ## Do alternatives, but that is in the future.
            uncertainty[i,0] = samples[mind]
            uncertainty[i,1] = np.nanmin(samples[inds], axis=0)
            uncertainty[i,2] = np.nanmax(samples[inds], axis=0)
        #from IPython import embed; embed()
        if squeeze:
            return uncertainty.squeeze()
        else:
            return uncertainty


    ## ATTENTION!
    ## This include does not expand to allow for things like 
    ## 'puff': [4e21, 6e21]
    ## Correct this!
    def indices(self, include, db='mdb'):
        return list(parse_dataframe(get(self, db), include).index)

    
    ##ATTENTION: Wfrong
    #def plot_sample(self, x, obj=None, **kwargs):
    ## ATTENTION: Labels????
    ##ATTENTION: Replace defaults by rcParams
    def plot_1dsample(self, x, obj, **kwargs):
        """
        """
        canvas = priority(['canvas', 'ax'], kwargs, None)
        fargs = priority('args', kwargs, [])
        fkwargs = priority('kwargs', kwargs, [])
        use_nan = priority('use_nan', kwargs, [])
        #ptype = priority(['plot','ptype'], kwargs, 'errorbar')
        ptype = priority(['plot','ptype'], kwargs, 'area')

        pretty=  {}
        prettyline=  {}
        prettyerror=  {}
        prettyarea={}
        color = priority(['color','colour','c'], kwargs, None)
        if color is not None:
            pretty['color'] = color
        prettyerror['ecolor'] = priority(['ecolor','ecolour','ec'], kwargs, None)
        #prettyline['lw'] = priority(['lw','linewidth'], kwargs, 1.0)
        prettyline['lw'] = priority(['lw','linewidth'],
                kwargs, plt.rcParams['lines.linewidth'])
        prettyerror['elinewidth'] = priority(['elw','elinewidth'], kwargs, None)
        prettyline['ls'] = priority(['ls','linestyle'], kwargs, '-')
        prettyerror['capsize'] = priority('capsize', kwargs, 0.0)
        prettyerror['capthick'] = priority('capthick', kwargs, 0.0)

        prettyline['ms'] = priority(['ms','markersize'], kwargs, 0.0)

        pretty['alpha'] = priority(['alpha'], kwargs, 1.0)
        #prettyarea['alpha'] = priority(['alpha_area'], kwargs, 1.0)
        prettyarea['alpha'] = priority(['alpha_area'], kwargs, 0.3)


        if canvas is None:
            #fig, canvas = plt.subplots()
            fig, canvas = initialize_canvas(**kwargs)

        ##FOR NOW
        ##Obj can either be function for sampling or a sample
        #if callable(obj):
        #    y = self.sample(obj,
        #        args=fargs, kwargs=fkwargs, use_nan=use_nan)
        #else:
        #    y = obj
        #FOR NOW

        ## So that only one thing is passed and the other is assumed
        ## to be just range of first index, the not 3 one.
        #if obj = None:
        #    obj = x
        #    x = obj.shape[0]
        if obj.ndim != 2:
            self.log.exception(
                "Obj ndim must be 2, and either one must be of size 3.")
        if obj.shape[0] == 3:
            y = obj.T
        else:
            y = obj



        if ptype=='fill' or ptype=='area':
            artist = []
            artist.append(canvas.fill_between(x, y[:,1], y[:,2],
                **update(pretty, prettyarea)))
            artist.append(canvas.plot(x, y[:,0], **update(pretty, prettyline)))

        else:
            ##Does NOT work with NaN correctly
            prettify= update(pretty, update(prettyline, prettyerror))
            artist =  canvas.errorbar(x, y[:,0],
                yerr=(np.abs(y[:,1:].T-y[:,0])), **prettify)

        return canvas, artist

