

# Common modules:
import sys
import os
import re
import numpy as np
from collections import OrderedDict, namedtuple
import itertools
import logging

# Quixote modules:
from .unstructured import UnstructuredData
from .rundir import RundirData
from .magnetic_geometries import USN, LSN
from .magnetic_geometries import Linear
from .magnetic_geometries import CDN
from ..tools import module_path, solps_property, priority
from ..tools import update, purge, read_config
from ..tools import create_log
from ..tools import extend, get
from ..tools import SimpleNamespace
from ..tools import solps_property2
#from ..extensions.unstructured_eqout import EqOutST

#Extensions

alog = create_log('quixote')

class ClassicalData(UnstructuredData):
    def __init__(self, *args, **kwargs):
        alog.debug(">>> Entering Classical __init__")
        super().__init__(*args, **kwargs)
        alog.debug("<<< Leaving  Classical __init__")
        #self._type = 'ClassicalData'
        #self.log = create_log('ClassicalData', {'ident':self.name})




    ##ATTENTION: For now only nvc, nci.
    ##ATTENTION: Could use self._dimensions and then get automatically
    ##the type, but it is not like a real timesaving or anything.
    def as_xy(self, var, mode='infer'):
        """ If mode == infer, then try to infer the dimension of the variable
        from ncv, nci, nvx, ...
        And give back the variable in (nx, ny, dims) form,
        with nx,ny= structured nx,ny (so +2)
        Face quantities will be given back as in solspy:
        first index is zeros, then (ix,iy) gives value on left/bottom face.
        """
        if isinstance(var, str):
            var = get(self, var)
        extra_dims = var.shape[1:]
        new_dims = extend(self.nx+2, self.ny+2, extra_dims)
        if var.dtype == float: #isinstance does not work here.
            xyvar = np.full(new_dims, np.nan, dtype=float)
        elif var.dtype == int: #isinstance does not work here.
            xyvar = np.full(new_dims, -999999, dtype=int)
        elif var.dtype == 'O': # Object type
            xyvar = np.full(new_dims, np.nan, dtype=object)
        else:
            xyvar = np.zeros(new_dims)

        if var.shape[0] == self.ncv:
            for icv in range(self.ncv):
                xyvar[self.icvxy[icv][0], self.icvxy[icv][1]] = var[icv] 

        elif var.shape[0] == self.nci:
            pass

        elif var.shape[0] == self.nfc:
            pass

        elif var.shape[0] == self.nvx:
            ## Make some sort of average??
            ## self.convert(self.bbvx, to='ncv') and then use that here.
            pass

        return xyvar

    @solps_property
    def mask(self):
        self._mask = Masks(self)

    #@solps_property
    #def eqout(self):
    #    self._eqout = EqOutST(self)


    # -------------------- Indices and locators -------------------------------

    @property
    def ixycv(self):
        """ (nx,ny): ix, iy ---> icv
        """
        return self.imapcv

    @property
    def icvxy(self):
        """ (ncv, 2):
            [:, 0] = ix
            [:, 1] = iy

        """
        return self.xycv


    @solps_property
    def xycv(self):
        self._xycv = np.zeros((self.ncv, 2), dtype=int)
        for icv in range(self.ncv):
            tmp = np.where(self.imapcv == icv)
            self._xycv[icv, 0] = tmp[0][0]
            self._xycv[icv, 1] = tmp[1][0]



    @solps_property
    def isepy(self):
        """
        """
        tmp = []
        for ift in extend(self.isepft):
            tmp.append(self.ftlbl[ift])
        if len(tmp)==1:
            self._isepy = tmp[0]
        elif len(np.unique(tmp))==1:
            self._isepy = np.unique(tmp)[0]
        else:
            self._isepy = np.array(tmp, dtype=int)



    ##ATTENTION: Correct?? Max is 96 while nft is 101.
    @solps_property
    def ixyft(self):
        """
        """
        self._ixyft = np.full((self.nx+2,self.ny+2), -999999, dtype=int)
        for ix in range(self.nx):
            for iy in range(self.ny):
                try:
                    self._ixyft[ix,iy] = self.icvft[self.ixycv[ix,iy]]
                except:
                    continue



    ##ATTENTION: Correct??
    @solps_property
    def iftxy(self):
        """ (nft, (ix,iy)):
        """
        self._iftxy = np.full((self.nft), -999999, dtype=object)
        for ift in range(self.nft):
            try:
                self._iftxy[ift] = self.icvxy[self.iftcv[ift]]
            except:
                continue



    @property
    def ixyfcx(self):
        """ (nx, ny): ifc index of left? face.
        Maybe expand to include both x faces.
        [:,0]: left
        [:,1]: right
        Probably no need since they are ordered, so we know
        how to easily obtain the other faces.
        """
        return self.imapfcx

    @property
    def ixyfcy(self):
        """ (nx, ny): ifc index of bottom? face.
        Maybe expand to include both y faces.
        [:,0]: bottom
        [:,1]: top
        Probably no need since they are ordered, so we know
        how to easily obtain the other faces.
        """
        return self.imapfcy


    @solps_property
    def ixyfc(self):
        """ (nx, ny): All the cell faces.
        """
        self._ixyfc = np.full((self.nx+2,self.ny+2), -999999, dtype=object)
        for ix in range(self.nx+2):
            for iy in range(self.ny+2):
                try:
                    self._ixyfc[ix,iy] = self.icvfc[self.ixycv[ix,iy]]
                except:
                    continue

    @solps_property
    def ixyvx(self):
        """ (nx, ny): All the vertices.
        """
        self._ixyvx = np.full((self.nx+2,self.ny+2), -999999, dtype=object)
        for ix in range(self.nx+2):
            for iy in range(self.ny+2):
                try:
                    self._ixyvx[ix,iy] = self.icvvx[self.ixycv[ix,iy]]
                except:
                    continue


    @solps_property
    def iompx(self):
        """ Use self.iomp which gives cv of rzomp line, and return
        ix values of those cv. Hopefully just 1, but one never knows.
        """

    @solps_property
    def iimpx(self):
        """ Use self.iomp which gives cv of rzomp line, and return
        ix values of those cv. Hopefully just 1, but one never knows.
        """


    # -------------------- Grid and vessel geometry ---------------------------

    @solps_property
    def imapcv(self):
        """(nx,ny): Mapping of (ix,iy) into corresponding icv cell.
        -999999 means not in use in unstructured.
        """
        self._read_signal('_imapcv', 'imapcv')
        self._imapcv = self._imapcv.astype(int) -1 #Python
        self._imapcv[self._imapcv==-1] = -999999

    @solps_property
    def imapfcx(self):
        """(nx,ny): Mapping of (ix,iy) into corresponding ifcx x-face.
        Left x-face?
        -1 means not in use in unstructured.
        """
        self._read_signal('_imapfcx', 'imapfcx')
        self._imapfcx = self._imapfcx.astype(int) -1 #Python
        self._imapfcx[self._imapfcx==-1] = -999999

    @solps_property
    def imapfcy(self):
        """(nx,ny): Mapping of (ix,iy) into corresponding ifcy y-face.
        Bottom y-face?
        -1 means not in use in unstructured.
        """
        self._read_signal('_imapfcy', 'imapfcy')
        self._imapfcy = self._imapfcy.astype(int) -1 #Python
        self._imapfcy[self._imapfcy==-1] = -999999

    @solps_property
    def imapvx(self):
        """(nx,ny): Mapping of (ix,iy) into corresponding ivx vertex.
        Which vertex, left bottom?
        -1 means not in use in unstructured.
        """
        self._read_signal('_imapvx', 'imapvx')
        self._imapvx = self._imapvx.astype(int) -1 #Python
        self._imapvx[self._imapvx==-1] = -999999



    def where(self, var):
        """ Add new variables to the to_update dict with
        'b2 file which contains them': {'new var': dims, ...}
        """
        to_update={}
        #to_update['b2fstate'] = {'alfalfa': (self.ncv)}
        return super().where(var, to_update = to_update)


    
class Masks(SimpleNamespace):
    def __init__(self, mother):
        """ use cvReg, fcReg, etc to make masks.
        """
        self.mother = mother
        self.log = mother.log

    def region(self, regs, dim='ncv', mode='or'):
        """ For now, only ncv.
        """
        regs = extend(regs)
        if dim == 'ncv':
            dim = self.mother.ncv
        else:
            self.log.exception(
                "Given dim '{0}' is not yet implemented.".format(dim))

        if mode == 'or':
            logic = np.logical_or
            tmp = np.zeros(dim, dtype=bool)
        elif mode == 'and':
            logic = np.logical_and
            tmp = np.ones(dim, dtype=bool)

        for reg in regs:
            tmp = logic(tmp, self.mother.cvreg == reg)

        return tmp



### ------------------------------------------------------------------------------

class ClassicalDataUSN(RundirData, ClassicalData, USN):
    class_type = 'ClassicalDataUSN'

    def __init__(self, ident=None, *args, **kwargs):
        #self.log = create_log('ClassicalDataUSN', {'ident':self.name})
        alog.debug(">>> Entering Classical USN __init__")
        super().__init__(ident, *args, **kwargs)
        alog.debug("<<< Leaving  Classical USN __init__")











class ClassicalDataLSN(RundirData, ClassicalData, LSN):
    class_type = 'ClassicalDataLSN'

    def __init__(self, ident=None, *args, **kwargs):
        #self.log = create_log('ClassicalDataUSN', {'ident':self.name})
        alog.debug(">>> Entering Classical LSN __init__")
        super().__init__(ident, *args, **kwargs)
        alog.debug("<<< Leaving  Classical LSN __init__")


    @property
    def iinn(self):
        """ For now, I guess"""
        return 1

    @property
    def iout(self):
        """ For now, I guess"""
        return self.nx-2






class ClassicalDataLinear(RundirData, ClassicalData, Linear):
    class_type = 'ClassicalDataLinear'

    def __init__(self, ident=None, *args, **kwargs):
        #self.log = create_log('ClassicalDataUSN', {'ident':self.name})
        alog.debug(">>> Entering Classical Linear __init__")
        super().__init__(ident, *args, **kwargs)
        alog.debug("<<< Leaving  Classical Linear __init__")


    @property
    def iups(self):
        """ For now, I guess"""
        return 1

    @property
    def itrg(self):
        """ For now, I guess"""
        return self.nx-2




class ClassicalDataCDN(RundirData, ClassicalData, CDN):
    class_type = 'ClassicalDataCDN'

    def __init__(self, ident=None, *args, **kwargs):
        alog.debug(">>> Entering Classical CDN __init__")
        super().__init__(ident, *args, **kwargs)
        alog.debug("<<< Leaving  Classical CDN __init__")



class ClassicalDataDDN(RundirData, ClassicalData):
    class_type = 'ClassicalDataDDN'

    def __init__(self, ident=None, *args, **kwargs):
        alog.debug(">>> Entering Classical DDN __init__")
        super().__init__(ident, *args, **kwargs)
        alog.debug("<<< Leaving  Classical DDN __init__")



class ClassicalDataDDNU(RundirData, ClassicalData):
    class_type = 'ClassicalDataDDNU'

    def __init__(self, ident=None, *args, **kwargs):
        alog.debug(">>> Entering Classical DDNU __init__")
        super().__init__(ident, *args, **kwargs)
        alog.debug("<<< Leaving  Classical DDNU __init__")



    
