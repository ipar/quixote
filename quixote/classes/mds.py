# Common modules:
import sys
import os
import re
import numpy as np
from collections import OrderedDict
import itertools

## Quixote modules:
from ..tools import module_path, solps_property, priority
from ..tools import update, purge, read_config, get
from ..tools import create_log

# =============================================================================
class MdsData(object):
    data_type = 'mds'
    def __init__(self, shot, *args, **kwargs):
        try:
            import MDSplus
        except:
            log = create_log("MdsData")
            log.error("MDSplus not found")
            return

        self.name = priority('name', kwargs, str(shot))
        super().__init__(*args, **kwargs)
        self.conn = MDSplus.Connection(self.config['Mdsplus']['connection'])
        self.conn_open = False
        self.shot = shot
        self.nshot = shot

##  EXTENSIONS ================================================================

##  IDENT PROPERTIES ==========================================================

    @solps_property
    def b2standalone(self):
        """????"""

    @solps_property
    def solpsversion(self):
        """str: SOLPS numerical version string."""
        self._read_signal('_solpsversion', 'solpsversion')


    @solps_property
    def directory(self):
        """str: Absolute path pointing towards run directory."""
        self._read_signal('_directory', 'directory')


##  GEOMETRY ==================================================================

    # -------------------- Structure ------------------------------------------


## SNAPSHOT ===================================================================
    # -------------------------------------------------------------------------

    # -------------------- Dimensions -----------------------------------------

    # -------------------- Indices and locators -------------------------------

    # -------------------- Plasma Characterization ----------------------------

    # -------------------- Densities ------------------------------------------
    
    # -------------------- Temperatures ---------------------------------------
    
    # -------------------- Pressures ------------------------------------------
    
    # -------------------- Velocities -----------------------------------------
    
    # -------------------- Fluxes and energies --------------------------------
    @solps_property
    def fhex(self):
        """(nx, ny): Poloidal electron heat flux through the x-face
        between the cell (ix, iy) and its left neighbour (ix-1, iy) [W].

        Notes
        -----
        fhex[0] = np.zeros(ny, ns) to complete the dimensions.
        """
        self._read_signal('_fhex', 'fhex')
        self._fhex = np.concatenate((np.zeros((1,self.ny)),
                                    self._fhex), axis = 0)

    @solps_property
    def fhey(self):
        """(nx, ny): Radial electron heat flux through the y-face
        between the cell (ix, iy) and its bottom neighbour (ix,iy-1) [W].

        Notes
        -----
        fhey[0] = np.zeros(nx,ns) to complete the dimensions.
        """
        self._read_signal('_fhey', 'fhey')
        self._fhey = np.concatenate((np.zeros((self.nx,1)),
                                    self._fhey), axis = 1)

    @solps_property
    def fhe(self):
        """(nx, ny, 2): Electron heat flux through cell faces [W].

        fhe[...,0]: poloidal flux through left face.
        Alias of :py:attr:`~fhex`.

        fhe[...,1]: radial flux through bottom face.
        Alias of :py:attr:`~fhey`.
        """
        self._fhe = np.zeros((self.nx,self.ny,2))
        self._fhe[:,:,0] = self.fhex
        self._fhe[:,:,1] = self.fhey

    @solps_property
    def fhix(self):
        """(nx, ny): Poloidal ion heat flux through the x-face
        between the cell (ix, iy) and its left neighbour (ix-1, iy) [W].

        Notes
        -----
        fhix[0] = np.zeros(ny, ns) to complete the dimensions.
        """
        self._read_signal('_fhix', 'fhix')
        self._fhix = np.concatenate((np.zeros((1,self.ny)),
                                    self._fhix), axis = 0)

    @solps_property
    def fhiy(self):
        """(nx, ny): Radial ion heat flux through the y-face
        between the cell (ix, iy) and its bottom neighbour (ix, iy-1) [W].

        Notes
        -----
        fhiy[0] = np.zeros(nx, ns) to complete the dimensions.
        """
        self._read_signal('_fhiy', 'fhiy')
        self._fhiy = np.concatenate((np.zeros((self.nx,1)),
                                    self._fhiy), axis = 1)

    @solps_property
    def fhi(self):
        """(nx, ny, 2): Ionn heat flux through cell faces [W].

        fhi[...,0]: poloidal flux through left face.
        Alias of :py:attr:`~fhix`.

        fhi[...,1]: radial flux through bottom face.
        Alias of :py:attr:`~fhiy`.
        """
        self._fhi = np.zeros((self.nx,self.ny,2))
        self._fhi[:,:,0] = self.fhix
        self._fhi[:,:,1] = self.fhiy

    @solps_property
    def fhjx(self):
        """(nx, ny): Poloidal electrostatic energy flux [W]"""
        self._read_signal('_fhjx', 'fhjx')
        self._fhjx = np.concatenate((np.zeros((1,self.ny)),
                                    self._fhjx), axis = 0)

    @solps_property
    def fhjy(self):
        """(nx, ny): Radial electrostatic energy flux [W]"""
        self._read_signal('_fhjy', 'fhjy')
        self._fhjy = np.concatenate((np.zeros((self.nx,1)),
                                    self._fhjy), axis = 1)

    @solps_property
    def fhj(self):
        """(nx, ny, 2): Electrostatic energy flux [W]"""
        self._fhj = np.zeros((self.nx,self.ny,2))
        self._fhj[:,:,0] = self.fhjx
        self._fhj[:,:,1] = self.fhjy

    @solps_property
    def fhmx(self):
        """(nx, ny, ns): Poloidal parallel kinetic energy flux [W]"""
        self._read_signal('_fhmx', 'fhmx')
        self._fhmx = np.concatenate((np.zeros((1,self.ny,self.ns)),
                                     self._fhmx), axis = 0)

    @solps_property
    def fhmy(self):
        """(nx, ny, ns): Radial parallel kinetic energy flux [W]"""
        self._read_signal('_fhmy', 'fhmy')
        self._fhmy = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                     self._fhmy), axis = 1)

    @solps_property
    def fhm(self):
        """(nx, ny, 2, ns): Parallel kinetic energy flux [W]"""
        self._fhm = np.zeros((self.nx,self.ny,2,self.ns))
        self._fhm[:,:,0,:] = self.fhmx
        self._fhm[:,:,1,:] = self.fhmy

    @solps_property
    def fhpx(self):
        """(nx, ny, ns): Poloidal potential energy flux [W]"""
        self._read_signal('_fhpx', 'fhpx')
        self._fhpx = np.concatenate((np.zeros((1,self.ny,self.ns)),
                                     self._fhpx), axis = 0)

    @solps_property
    def fhpy(self):
        """(nx, ny, ns): Radial potential energy flux [W]"""
        self._read_signal('_fhpy', 'fhpy')
        self._fhpy = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                     self._fhpy), axis = 1)

    @solps_property
    def fhp(self):
        """(nx, ny, 2, ns): Potential energy flux [W]"""
        self._fhp = np.zeros((self.nx,self.ny,2,self.ns))
        self._fhp[:,:,0,:] = self.fhpx
        self._fhp[:,:,1,:] = self.fhpy

    @solps_property
    def fhtx(self):
        """(nx, ny): Poloidal total energy flux [W]"""
        self._read_signal('_fhtx', 'fhtx')
        self._fhtx = np.concatenate((np.zeros((1,self.ny)),
                                     self._fhtx), axis = 0)

    @solps_property
    def fhty(self):
        """(nx, ny): Radial total energy flux [W]"""
        self._read_signal('_fhty', 'fhty')
        self._fhty = np.concatenate((np.zeros((self.nx,1)),
                                    self._fhty), axis = 1)

    @solps_property
    def fht(self):
        """(nx, ny, 2): Total energy flux [W].

        Notes
        -----
        fht = fhe + fhi (electron and ion heat fluxes)
            + fhj (electrostatic energy flux)
            + fhm (kinectic energy flux)
            + fhp (potential energy flux)
           [+ fnt (addition to heat fluxes, only for SOLPS-ITER)]
        """
        self._fht = np.zeros((self.nx,self.ny,2))
        self._fht[:,:,0] = self.fhtx
        self._fht[:,:,1] = self.fhty


    @solps_property
    def fchx(self):
        """(nx, ny): Poloidal current through left face [A]."""
        self._read_signal('_fchx', 'fchx')
        self._fchx = np.concatenate((np.zeros((1,self.ny)),
                                    self._fchx), axis = 0)

    @solps_property
    def fchy(self):
        """(nx, ny): Radial current through bottom face [A]."""
        self._read_signal('_fchy', 'fchy')
        self._fchy = np.concatenate((np.zeros((self.nx,1)),
                                    self._fchy), axis = 1)

    @solps_property
    def fch(self):
        """(nx, ny, 2): Total current [A]."""
        self._fch = np.zeros((self.nx,self.ny,2))
        self._fch[:,:,0] = self.fchx
        self._fch[:,:,1] = self.fchy


    #ATTENTION: Add docstrig
    #ATTENTION: STILL NOT SAME AS RUNDIR
    @solps_property
    def fnt(self):
        """
        .. warning::
            `fnt` is manually calculated and remains untested.
        """
        self.log.warning("self.fnt is still unstested.")
        fnt = np.zeros((self.nx,self.ny,2))
        for ny in range(self.ny):
            for nx in range(self.nx):

                # poloidal component
                if self.leftix[nx,ny] >= 0:
                    for ns in range(self.ns):
                        fnt[nx,ny,0] += (0.5*self.fna[nx,ny,0,ns]*
                                self.qe*(self.ti[self.leftix[nx,ny],
                                                  self.leftiy[nx,ny]]
                                        + self.ti[nx,ny]))

                    fnt[nx,ny,0] += (0.5*self.fne[nx,ny,0]*
                               self.qe*(self.te[self.leftix[nx,ny],
                                                self.leftiy[nx,ny]]
                                        + self.te[nx,ny]))
                else:
                    fnt[nx,ny,0] = 0

                # radial component
                if self.bottomiy[nx,ny] >= 0:
                    for ns in range(self.ns):
                        fnt[nx,ny,1] += (0.5*self.fna[nx,ny,1,ns]*
                               self.qe*(self.ti[self.bottomix[nx,ny],
                                                self.bottomiy[nx,ny]]
                                        + self.ti[nx,ny]))

                    fnt[nx,ny,1] += (0.5*self.fne[nx,ny,1]*
                               self.qe*(self.te[self.bottomix[nx,ny],
                                                self.bottomiy[nx,ny]]
                                        + self.te[nx,ny]))
                else:
                    fnt[nx,ny,1] = 0

        self._fnt = fnt



    @solps_property
    def fmox(self):
        """(nx, ny, ns): Total momentum flux on the left face
        of (ix,iy) [N]
        """
        self._read_signal('_fmox', 'fmox')
        self._fmox =  np.concatenate((np.zeros((1,self.ny,self.ns)),
                                     self._fmox), axis = 0)

    @solps_property
    def fmoy(self):
        """(nx, ny, ns): Total momentum flux on the bottom face
        of (ix, iy) [N]
        """
        self._read_signal('_fmoy', 'fmoy')
        self._fmoy = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                    self._fmoy), axis = 1)


    # -------------------- Coefficients ---------------------------------------

    # -------------------- Rates, losses/sources and residuals ----------------

    # -------------------- EIRENE triangular grid -----------------------------


##  B2 INPUT AND RUN.LOG ======================================================


##  FUNCTIONS =================================================================

    # -------------------- Operations -----------------------------------------


