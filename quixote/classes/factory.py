import os
import numpy as np

from ..tools import priority, read_config, create_log
from ..tools import parsers


from .unstructured import UnstructuredData
from .classical import ClassicalDataUSN, ClassicalDataLSN
from .classical import ClassicalDataCDN, ClassicalDataDDN, ClassicalDataDDNU
from .classical import ClassicalDataLinear
from .magnetic_geometries import RundirDataLSN, RundirDataUSN
from .magnetic_geometries import RundirDataCDN, RundirDataDDN, RundirDataDDNU
from .magnetic_geometries import RundirDataLinear
from .magnetic_geometries import RundirDataLimiter




#def SolpsData(ident=None, *args, **kwargs): #Something like this required for None = '.'
# But then no args allowed??

def SolpsData(ident, *args, **kwargs):
    """ Problems if people pass stuff as args?
    """
    log = create_log('SolpsData', {'ident':ident})

    # Need for speed: Reuse config as much as possible.
    config = priority(['config', 'cfg'], kwargs, None)
    if config is None or not isinstance(config, dict):
        config = read_config()
    kwargs['config'] = config

    try:
        nshot = int(ident)
        solps_type = 'mds'
        log.error("MdsData not yet implemented.")
        #return mdsclass
        return None
    except:
        path = os.path.abspath(ident)
        solps_type = 'rundir'


    ## RundirData type class -------------------------- 
    grid_type = priority(['grid','gtype'], kwargs, None)
    if not grid_type:
        grid_type = gtype(path, config, log)

    mag = priority(['mag','magnetic_geometry','maggeom'], kwargs, None)
    if not mag:
        mag, reuse = infer_magnetic_geometry(
            ident, path, log, config, grid_type)
    

    unstructured_classes = {
            'lsn': UnstructuredData,
            'usn': UnstructuredData,
            'cdn': UnstructuredData,
            'ddn': UnstructuredData,
            'ddnu':UnstructuredData}
    classical_classes = {
            'linear': ClassicalDataLSN,
            'lsn': ClassicalDataLSN,
            'usn': ClassicalDataUSN,
            'cdn': ClassicalDataCDN,
            'ddn': ClassicalDataDDN,
            'ddnu':ClassicalDataDDNU}
    structured_classes = {
            'linear': RundirDataLinear,
            'limiter': RundirDataLimiter,
            'lsn': RundirDataLSN,
            'usn': RundirDataUSN,
            'cdn': RundirDataCDN,
            'ddn': RundirDataDDN,
            'ddnu':RundirDataDDNU}



    ## Make sure that the reused quantities are, in fact, correctly calculated.
    ## It is not a massive increase in speed, worth the risk?
    #if True:
    if False:
        try:
            kwargs['reuse'] = reuse
        except:
            pass

    if grid_type == 'structured':
        return structured_classes[mag](ident, *args, **kwargs)

    elif grid_type == 'unstructured':
        classical = priority('classical', kwargs, is_classical(path, config, log))
        if classical:
            return classical_classes[mag](ident, *args, **kwargs)
        else:
            return unstructured_classes[mag](ident, *args, **kwargs)





### ------------- FUNCTIONS ----------------------------------------

def infer_magnetic_geometry(ident, path, log, config, grid_type):
    """ Not sure if this logic will work for non-classical grids.
    Do they also have regions?
    """
    b2file = find_files(path, config, ['b2fgmtry'], log)
    if b2file:
        ## Number of xpoints and regions
        if grid_type=='structured':
            nx, ny = get_basic_dimensions(path, config, log, grid_type)
            tmp = parsers.b2(b2file, 'nnreg', 3)
            nreg = tmp[0]
            fcreg = tmp[1] + tmp[2]
        elif grid_type=='unstructured':
            ncv,nfc = get_basic_dimensions(path, config, log, grid_type)
            nreg = np.max(parsers.b2(b2file, 'cvReg', (ncv))).astype(int)
            fcreg = np.max(parsers.b2(b2file, 'fcReg', (nfc))).astype(int)


        if nreg == 4:
            log.info("Inferred Single Null configuration.")
            maggeom =  'sn'
        elif nreg == 8:
            if fcreg == 27:
                log.info("Inferred Disconnected Double Null configuration.")
                maggeom = 'ddn'
            elif fcreg == 26:
                log.info("Inferred Connected Double Null configuration.")
                return 'cdn', {}
        elif nreg == 7:
            log.error("Inferred Snowflake minus configuration,")
            log.error("but SF is not yet implemented.")
            return 'sf-', {} ## This leads to error.
        elif nreg == 2:
            log.info("Inferred Limiter configuration.")
            return 'limiter', {}
        elif nreg == 1:
            log.info("Inferred Linear configuration.")
            return 'linear', {}
        else:
            log.exception("Configuration with {0} volumetric".format(nreg)+
                    " regions not implemented.")
            raise Exception


        if grid_type=='structured':
            if  maggeom == 'sn':
                ixp = int(parsers.b2(b2file, 'leftcut', 1)[0]+1)
                oxp = int(parsers.b2(b2file, 'rightcut', 1)[0])

                isep = int(parsers.b2(b2file, 'topcut', 1)[0]+1)
                iinn = 1
                iout = nx-2

                z = np.average(parsers.b2(b2file, 'cry', (nx, ny, 4)), axis=2)
                pos_xpt = (z[ixp,isep] + z[oxp,isep])/2.0
                pos_tg = (z[iout, isep] + z[iinn, isep])/2.0
                if pos_xpt > pos_tg:
                    return 'lsn', {}
                else:
                    return 'usn', {}

            ## ATTENTION: MIGHT BE WRONG AT THE MOMENT. CHECK INDICES.
            ## At least for now, it seems to work for DDNU.
            elif maggeom == 'ddn':
                ixp = int(parsers.b2(b2file, 'leftcut', 2)[1])
                oxp = int(parsers.b2(b2file, 'rightcut', 2)[1]+1)

                isep = np.min(parsers.b2(b2file, 'topcut', 2).astype(int))+1
                reg = parsers.b2(b2file, 'region', (nx, ny, 3))
                iinn = np.where(reg[...,1] == 4)[0][0] -1
                iout =  np.where(reg[...,1] == 5)[0][0]

                z = np.average(parsers.b2(b2file, 'cry', (nx, ny, 4)), axis=2)
                pos_xpt = (z[ixp,isep] + z[oxp,isep])/2.0
                pos_tg = (z[iout, isep] + z[iinn, isep])/2.0
                if pos_xpt > pos_tg:
                    return 'ddn', {}
                else:
                    return 'ddnu', {}


        ##ATTENTION: COMPLETELY WRONG, but useful for now
        elif grid_type=='unstructured':
            #return 'usn'
            maggeom, reuse = maggeom_unstructured(path, config, log)
            return maggeom, reuse



    else:
        log.exception("b2fgmtry is required to infer magnetic geometry")
        raise Exception






def get_basic_dimensions(path, config, log, grid_type):
    b2file = find_files(path, config, ['b2fstate', 'b2fstati', 'b2fgmtry'], log)
    with open(b2file, 'r') as fg:
        for i in range(2):
            fg.readline()
        tmp = fg.readline().split()
        if grid_type=='structured':
            nx = int(tmp[0]) + 2 # Only valid for structured
            ny = int(tmp[1]) + 2 # Only valid for structured
            return nx, ny
        elif grid_type=='unstructured':
            if os.path.basename(b2file) == 'b2fgmtry':
                ncv = int(tmp[2])
                nfc = int(tmp[3])
            else:
                ncv = int(tmp[0])
                nfc = int(tmp[1])
            #Get nfc too
            return ncv, nfc




def find_files(path, config, names, log):
    if isinstance(names, str):
        names = [names]
    for locs in config['Files']['location']:
        for name in names: #Not all the files in config priority have nC[vi].
            try:
                fpath = os.path.abspath(os.path.join(path, locs, name))
                if os.path.isfile(fpath) and os.stat(fpath).st_size > 0:
                    break
            except:
                continue
        else:
            continue
        break
    else:
        log.exception(
            "To use SolpsData, b2fgmtry, b2fstate,"
            +" b2fstati, or b2fplasmf are needed.")
        return None
    return fpath



def gtype(path, config, log):
    fpath = find_files(
        path, config, ['b2fgmtry', 'b2fstate', 'b2fstati', 'b2fplasmf'], log)
    with open(fpath, 'r') as fd:
        fd.readline()
        if fd.readline().split()[3].startswith('nC'):
            return 'unstructured'
        else:
            return 'structured'


def is_classical(path, config, log):
    fpath = find_files(path, config, ['b2fgmtry'], log)
    with open(fpath, 'r') as fd:
        for i in range(5):
            fd.readline()
        try:
            return bool(fd.readline())
        except:
            log.exception("Value of isClassicalGrid in bfgmtry is not valid.")
            return None


## ATTENTION: Reduce amount of reads. There must be a way to
## use less variables to get the ultimate result.
def maggeom_unstructured(path, config, log):
    """ A lot of b2_parser, a lot of indices here and there, but it can be done.
    """
    fgmtry = find_files(path, config, ['b2fgmtry'], log)

    reuse = {}

    with open(fgmtry) as fd:
        fd.readline()
        fd.readline()
        dims = fd.readline().split()
        ncv = int(dims[2])
        nfc = int(dims[3])
        nvx = int(dims[4])
        nfs = int(dims[5])

        fd.readline()
        dims = fd.readline().split()
        nmxcv = int(dims[2])


    vxy = parsers.b2(fgmtry, 'vxY', (nvx))
    vxcv = parsers.b2(fgmtry, 'vxCv', (nmxcv)).astype(int) -1
    vxcvp = parsers.b2(fgmtry, 'vxCvP', (nvx,2)).astype(int)
    vxcvp[:,0] = vxcvp[:,0]-1 #Python index

    reuse['_vxy'] = vxy
    reuse['_vxcv'] = vxcv
    reuse['_vxcvp'] = vxcvp

    ivxcv = []
    for ivx in range(nvx):
        tmp = []
        for icv in range(vxcvp[ivx,1]):
            tmp.append(int(vxcv[vxcvp[ivx,0]+icv]))
        ivxcv.append(tmp)

    ixpoint = []
    for i, vert in enumerate(ivxcv):
        if len(vert) ==8:
            ixpoint.append(i)

    reuse['_ixpoint'] = ixpoint


    if len(ixpoint) ==1:
        ## Assuming that O-point is at z=0
        if vxy[ixpoint][0] > 0:
            return 'usn', reuse
        else:
            return 'lsn', reuse

    #return 'cdn', reuse ### WRONG!!!!!!
    cvregions = {
        'core': [1,5],
        'sol': [2,6],
        'div': [3,4,7,8]
        }

    fsfc = parsers.b2(fgmtry, 'fsFc', (nfc)).astype(int) -1
    fsfcp = parsers.b2(fgmtry, 'fsFcP', (nfs,2)).astype(int)
    fsfcp[:,0] = fsfcp[:,0]-1 #Python index
    fspsi = parsers.b2(fgmtry, 'fsPsi', (nfs))
    cvreg = parsers.b2(fgmtry, 'cvReg', (ncv)).astype(int)
    fccv = parsers.b2(fgmtry, 'fcCv', (nfc,2)).astype(int)-1
    fcvx = parsers.b2(fgmtry, 'fcVx', (nfc,2)).astype(int)-1

    reuse['_fsfc'] = fsfc
    reuse['_fsfcp'] = fsfcp
    reuse['_fspsi'] = fspsi
    reuse['_cvreg'] = cvreg
    reuse['_fccv'] = fccv
    reuse['_fcvx'] = fcvx


    ifsfc = []
    for ifs in range(nfs):
        fc = []
        iface = fsfcp[ifs,0]
        for ifc in range(fsfcp[ifs,1]):
            try:
                fc.append(fsfc[iface+ifc])
            except:
                pass
        ifsfc.append(np.array(fc))
    ifsfc = np.array(ifsfc, dtype=object)

    reuse['_ifsfc'] = ifsfc

    ifcfs = np.full((nfc), -999999, dtype=int)
    for ifs in range(nfs):
        faces = ifsfc[ifs]
        for face in faces:
            ifcfs[face] = ifs

    reuse['_ifcfs'] = ifcfs



    fsfcreg = []
    for ifs in range(nfs):
        freg = []
        regs = cvreg[fccv[ifsfc[ifs]]]
        for pair in regs:
            if pair[0] == pair[1]:
                freg.append(pair[0])
            else:
                freg.append(-1)
        fsfcreg.append(np.array(freg))
    fsfcreg = np.array(fsfcreg, dtype=object)


    reuse['_fsfcreg'] = fsfcreg



    isep = []
    for ifs in range(nfs):
        if np.any(np.in1d(fsfcreg[ifs], -1)):
            isep.append(ifs)

    reuse['_isep'] = isep

    if len(isep) ==2:
        return 'cdn', reuse
        ## For now this is enough, as I do not have any DDN/U to work.

#    ivxfc = []
#    for ivx in range(self.nvx):
#        ivxfc.append(np.where(ifcvx == ivx)[0])
#    reuse['_ivxfc'] = ivxfc
#
#    fsxpoint = 

    






