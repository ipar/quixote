import os
import sys
import numpy

class RungLog(object):
    def __init__(self, path, *args, **kwargs):
        """ This class contains all extracted information
        from the run.log found in path.
        Things like strata, recyclings, etc.
        Maybe in the future maybe allow for checkint output.000x
        and patch them together for mpi runs.
        """

    def tflux(self):
        """ Just copy the strata file that I created.
        """
