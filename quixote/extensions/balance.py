"""
It seems that balance.nc finally works!
"""

import os
import numpy as np
import scipy.io as io
import copy


class Balance(object):
    def __init__(self, path):
        if not isinstance(path, str):
            raise Exception("'path' must be a valid path to a balance.nc file")

        with io.netcdf_file(path, mmap=False) as fl:
            self.__dict__.update(copy.deepcopy(fl.dimensions))
            self.nx = self.nx_plus2
            self.ny = self.ny_plus2
            for k, v in fl.variables.items():
                if len(v.data) == 1:
                    self.__dict__.update({k: copy.deepcopy(v.data)[0]})
                else:
                    self.__dict__.update({k: copy.deepcopy(v.data).T})
                self._create_alias(k)

            self._get_common()




    def _get_common(self):
        self.r = self.crx.copy()
        self.z = self.cry.copy()
        self.cr = np.mean(self.r, 2)
        self.cz = np.mean(self.z, 2)
        return

    def _create_alias(self, k):
        tmp =  k.split('_')
        if len(tmp)==1:
            return
        else:
            ###Some quantities actually need the distinction
            #if tmp[0].startswith('b2') and tmp[0] != 'b2mndr':
            #    tmp = tmp[1:]
            if tmp[-1] == 'bal':
                tmp = tmp[:-1]
            tmp = "_".join(tmp)
            if tmp != k:
                setattr(self, tmp, getattr(self, k))
        return



    def particle(self, include, exclude):
        pass


    def find(self, substring):
        for k in self.__dict__.keys():
            if substring in k:
                print(k)




if __name__ == '__main__':
    #path = '/data1/ip3/solps-runs/308/mastu/ipar_mastu_cd_45470_drifts/puff=4.00e21_pump=0.001_drifts/balance.nc'
    path = '/data1/ip3/solps-runs/308/mastu/sxd-46860/exp-scans-drifts-ff-tp=uni-avg-1-lfs-bt=0.66/puff=25.00e21_fnacore=5.00e21_pheat=6.00MW/balance.nc'
    tmp = Balance(path)


