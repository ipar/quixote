
## Actually, for unstructured, as_sts can be helpful, since it could be
## a good way to define targets, etc, but their 3A definition.
## But that is in general, so make a function like as_xy in unstructure
## To convert any ncv, nfc, etc, to nsts (a list of ncvs, etc, corresponding
## to that 3A



import numpy as np

# Quixote modules
from ..tools import solps_property
from ..tools import create_log
from ..tools import extend, get
from ..tools import parsers

alog = create_log('Wlld')


#def Wlld(mother):
#    if mother.grid_type == 'structured':
#        if mother.branch == '3.0.8': 
#            return type('Wlld',
#                (EquationsOutputST, EqOut5pt), {})(mother)
#        elif mother.branch == '3.1.0': 
#            return type('EquationsOutput',
#                (EquationsOutputST, EqOut9pt), {})(mother)
#
#    elif mother.grid_type == 'unstructured':
#        return type('EquationsOutput', (EquationsOutputUS, ), {})(mother)
#
#    else:
#        alog.exception(
#        "Grid Data Type must be either structured or unstructured")




class WlldBase(object):
    """ 
    """
    def __init__(self, mother):
        self.mother = mother
        self.f44 = self.mother.avail['fort.44']
        self.log = mother.log
        self._get_eirdiag()
        self._get_gdims()



##### ------------------ RES quantities ---------------------------------------

    @solps_property
    def sarea_res(self):
        tmp = parsers.fort44(self.f44, 'sarea_res', infere_dims=False)
        #tmp = self.shape_nsts(tmp)
        self._sarea_res = self.as_xy(tmp).squeeze()

    @solps_property
    def wldna_res(self):
        """ (nx, ny, natm): Incident molecular flux """
        tmp = parsers.fort44(self.f44, 'wldna_res', infere_dims=False)
        self._wldna_res = self.as_xy(tmp)


    @solps_property
    def wldnm_res(self):
        """ (nx, ny, nmol): Incident molecular flux """
        tmp = parsers.fort44(self.f44, 'wldnm_res', infere_dims=False)
        self._wldnm_res = self.as_xy(tmp)





    @solps_property
    def ewlda_res(self):
        """ (nx, ny, natm): Incident atomic energy flux. """
        tmp = parsers.fort44(self.f44, 'ewlda_res', infere_dims=False)
        self._ewlda_res = self.as_xy(tmp)

    @solps_property
    def ewldea_res(self):
        """ (nx, ny, natm): Emitted atomic energy flux. """
        tmp = parsers.fort44(self.f44, 'ewldea_res', infere_dims=False)
        self._ewldea_res = self.as_xy(tmp)

    @solps_property
    def ewldm_res(self):
        """ (nx, ny, nmol): Incident molecular energy flux. """
        tmp = parsers.fort44(self.f44, 'ewldm_res', infere_dims=False)
        self._ewldm_res = self.as_xy(tmp)

    @solps_property
    def ewldem_res(self):
        """ (nx, ny, nmol): Emitted molecular energy flux. """
        tmp = parsers.fort44(self.f44, 'ewldem_res', infere_dims=False)
        self._ewldem_res = self.as_xy(tmp)

    @solps_property
    def ewldmr_res(self):
        """ (nx, ny): Recombination of atoms to molecules.
        """
        tmp = parsers.fort44(self.f44, 'ewldmr_res', infere_dims=False)
        self._ewldmr_res = self.as_xy(tmp)


    @solps_property
    def recon_fhntd (self):
        """ (nx, ny): Neutral energy flux (density? I think?), in [W/m2]?
        Notes
        -----
        It seems that there is already a value for this.
        """

        self._recon_fhntd = np.zeros((self.mother.nx, self.mother.ny))
        for ld,factor in zip(['ewlda_res', 'ewldm_res', 'ewldea_res', 'ewldem_res', 'ewldmr_res'], [1,1,-1,-1,1]):
            tmp = get(self, ld)*factor
            if tmp.ndim ==3:
                tmp = np.sum(tmp, axis=2)
                
            self._recon_fhntd += tmp


    @property
    def fhrfd(self):
        """ (nx, ny): Kinetic energy carried away by recycling
        atoms and molecules, in [W/m2]??
        """
        return self.ewldrp_res

    @solps_property
    def ewldrp_res(self):
        """ (nx, ny): Kinetic energy of reflected neutrals originated from ions [W/m2]

        Notes
        -----
        Definition from include/EIRDIAG.H
        """
        tmp = parsers.fort44(self.f44, 'ewldrp_res', infere_dims=False)
        self._ewldrp_res = self.as_xy(tmp).squeeze()



    @property
    def fhntd(self):
        """ (nx, ny): Total energy wall load density from Eirene particles [W/m2].
        """
        try:
            return self.ewldt_res
        except:
            return self.recon_fhntd

    ## ATTENTION: I think that the indices might be wrong. They are displaced by one??
    ## Load not in case.iout but in case.iout+1, maybe it is really in the guard cell
    ## not in the real cell.
    ## However for case.iout2 it works
    @solps_property
    def ewldt_res(self):
        """ (nx, ny): Total energy wall load density from Eirene particles [W/m2].
        """
        tmp = parsers.fort44(self.f44, 'ewldt_res', infere_dims=False)
        self._ewldt_res = self.as_xy(tmp).squeeze()



    @solps_property
    def net_fhntd(self):
        """ (nx, ny): NET energy wall load density from Eirene particles [W/m2].
        """
        self._net_fhntd = self.fhntd-self.ewldrp_res




    @solps_property
    def wldspt_res(self):
        """ (nx, ny): Flux of sputtered wall material """
        tmp = parsers.fort44(self.f44, 'wlspt_res', infere_dims=False)
        self._wldspt_res = self.as_xy(tmp)


    @solps_property
    def wldspta_res(self):
        """ (nx, ny, natm): Sputtered flux for each type of emitted atom """
        tmp = parsers.fort44(self.f44, 'wlspta_res', infere_dims=False)
        self._wldspta_res = self.as_xy(tmp)


    @solps_property
    def wldsptm_res(self):
        """ (nx, ny, nmol): Sputtered flux for each type of emitted molecule """
        tmp = parsers.fort44(self.f44, 'wlsptm_res', infere_dims=False)
        self._wldsptm_res = self.as_xy(tmp)



        ## ATTENTION: OTHER DIMENSIONS
    @solps_property
    def srcml(self):
        """ (nx, ny): Power loss due to molecules [W/m2]. """
        tmp = parsers.fort44(self.f44, 'srcml')
        self._srcml = tmp

    @solps_property
    def edissml(self):
        """ (nx, ny): Power loss due to molecular dissociation [W/m2]. """
        tmp = parsers.fort44(self.f44, 'edissml')
        self._edissml = tmp



##### ---------------- OTHER quantities ---------------------------------------

    









    @solps_property
    def b2cells(self):
        self._b2cells = []
        #for i in range(self.nsts):

#    ## ATTENTION: Make sure that we partition correctly 
#    ## Variables taht are e.g. nds*2
#    ## This should return: list([b2cells,(extradimensions)]) with len = nsts
#    ## So target1 would be list[1][b2cells, [D,C]]
#    ## List bceause of ragged array, but maybe just ragged array
#    def shape_nsts(self, var):
#        extra = len(var)//self.nds
#        tmp = np.zeros(self.nsts, dtype='object')
#        ## It seems that JaeSun did it in the reverse way!
#        var = var.reshape((extra,self.nds), order='F').T
#        from IPython import embed; embed()
#        #for i, diag in enumerate(self.eirdiag):
#        #    if diag[1] == 1: #Radial cell
#        #        inds = np.arange(
#        #    elif diag[1] ==2: #Poloidal cell
#        #    else:
#        #        continue
#        #    pass


    ## ATTENTION: Make sure that we partition correctly 
    ## Variables taht are e.g. nds*2
    ## This should return: list([b2cells,(extradimensions)]) with len = nsts
    ## So target1 would be list[1][b2cells, [D,C]]
    ## List bceause of ragged array, but maybe just ragged array
    def as_xy(self, var):
        extra = len(var)//self.nds
        tmp = np.zeros(self.nsts, dtype='object')
        ## It seems that JaeSun did it in this way! (The reverse way?)
        var = var.reshape((extra,self.nds), order='F').T
        result = np.zeros(extend(self.gdims, extra))
        for i, diag in enumerate(self.eirdiag):
            posind = diag[2]
            liminds = np.arange(diag[3],diag[4]+1)
            ## Correct limit of arange?? First or last not included??
            ## Check by checking definitions of core, targets, etc
            ## INN2 NOT WORKING? Just take information from MOTHER
            ## Is it not working really? Or is inn2+1 required because
            ## bottom flux??
            value = var[np.arange(diag[0], diag[0]+len(liminds))]
            if diag[1] == 1:
                result[liminds, posind]  = value 
            elif diag[1] ==2:
                result[posind, liminds] = value
            else:
                continue
        return result

    #def as_strata(self,var): ## For those variables taht are (0), ...
    ## Use grep or something to find all instances of ewlda(*), instead of
    ##requiring strata






    def _get_gdims(self):
        with open(self.mother.avail['fort.44'], 'r') as fd:
            tmp = fd.readline().split()
            if len(tmp) == 3:
                self.gdims = (int(tmp[0]), )
            elif len(tmp) == 4:
                self.gdims = (int(tmp[0])+2, int(tmp[1])+2)
        return




    ## ATTENTION: Does one need to add +1 anywhere for Python indices??
    ## In principle it seems that no, but then again, at core, the radial index
    ## is 0, and that is the core. That might the the dummy index, because then
    ## At north, 146 is correspondingly correctly pyhon[-2], not python[-1]

    ## Maybe eirdiag[2] should be +1 to indicate the correct reading of fluxes??
    ## Since flux[0] or flux[:,0] = 0, but flux[-1] or flux[:,-1] is not.
    ## If all quantities of nds are fluxes, that would make sense
    def _get_eirdiag(self):
        """ From modules/b2mod_eirdiag.F
        NEUTRAL FLUXES, SPATIALLY RESOLVED ON NON-DEFAULT STANDARD SURFACES (NDS)
        First dimension: index of atom or molecule. Second dimension
        is the index of surface elements, controlled by array 'eirdiag_nds_ind'

         See WNEUTRAL_FLUXES for the description
         eirdiag_nds_ind(:), 
         eirdiag_nds_typ(:), 
         eirdiag_nds_srf(:),
         eirdiag_nds_start(:), 
         eirdiag_nds_end(:)

        Indices represent the mapping from the (usually) single 3A surface
        to the multitude of corresponding b2cells, providing some sort of
        cumulative b2 cells which have a corespondance to 3A.
        If all surfaces in 3A had an extension of only one B2 cell,
        then len(ind)==len(3A)

        Meaning of the arrays which are written to the fort.44 file
        
        eirdiag_nds_typ    : type of the surface
                             1: poloidal surface (Y=CONST)
                             2: radial surface (X=CONST) 
        eirdiag_nds_srf    : index of the corresponding radial or poloidal
                             SURFACE on which the distribution is given 
        eirdiag_nds_start  : index of the first CELL on the surface '..._srf'
        eirdiag_nds_end    : ... of the last CELL ...
        
        _sfr, _start and _end are given in B2 indexing. 
        '_srf' corresponds to the indexing of B2 arrays with
              fluxes (CELL-1 for outer wall and target).
        
        eirdiag_nds_ind(IS) : is the index AFTER WHICH the data for
              surface IS is started in arrays '*_res'.
                              IF _ind(IS)<0 then this surface is skipped
            

        Notes
        -----
        NDS seems to be the block 3A+1 (+2, but then one is ignored in _ind)
        In manual, it says (5 x NSTS + 1), with NSTS being 3A, so yeah.
        In example, NSTS = 12, but self.nsts=13. This seems to be
        because the +1 of NDS is ignorable?

        
        """
        self.eirdiag_tags = ['ind', 'typ', 'srf', 'start', 'end']
        tmp = parsers.fort44(self.mother.avail['fort.44'],
            'eirdiag', infere_dims=False).astype(int)

        self.nsts = (len(tmp)-1)//5
        self.nds = tmp[self.nsts]
        tmp = np.delete(tmp, self.nsts)
        self.eirdiag = tmp.reshape((self.nsts,5), order='F')
        self.eirdiag[:,2]+=1
        return 





