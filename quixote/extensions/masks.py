""" Idea:
    Create to_target masks, for example, to_out in which cells are
    multiplied by either +1 or -1 in order to direct fluxes towards out.
    Would be useful for balance routines where ix sign changes depending
    on magnetic configuration.
"""
#Common modules
import numpy as np

# Quixote modules
from ..tools import solps_property
from ..tools import priority
from ..tools import extend
from ..tools import update




class BaseMasks(object):

    def __init__(self, mother):
        self.mother = mother
        self.log = self.mother.log


## ============================================================================
##ATTENTION: allow for 'sep+N' as string, meaning the indices +N
## This is mostly useful for when using fsreg['sep+N'], but it is VERY
## useful.
class UnstructuredMasks(BaseMasks):
    """
        cvregions, xregions, and yregions come from 
        magnetic geometries already.

        Find a way to calculate the boundary of a mask (e.g. for balance).
    """

    ##ATTENTION: MOde intersection does not work, at least for ft
    def icv(self, cvreg='all', fcreg=[],  cv_and=None, ftreg=[], fsreg=[],
            fclbl=[], cv=[], fc=[], ft=[], fs=[],**kwargs):
        """ Default: 
            - Regs of same type: OR
            - Regs of different type: AND
            - cv_and takes cv(or... or...) AND cv_and
              e.g. icv(['core','sol'], cv_and='lfs')

        Allow ncvreg, nfcreg, etc? As in not_cvreg to set that reg
        to False.

        Allow for 'sep+-N', and mostly any ft/fs/fc. Not useful for cv or vx.
        Logic needs to be worked out a bit.

        Allow for mode to be boolean or dict of boolean, dictating what type of
        boolean applies to which type of region??
        """

        indices = priority(['inds', 'indices'], kwargs, False)
        real_cells = priority(['rlcl', 'real', 'real_cells'], kwargs, True)

        #MODE=INTERSECTION DOES NOT WORK, at least for ft.
        mode = priority(['mode', 'logic'], kwargs, 'combination')
        if mode in ['combination', 'union', 'combine', 'or']:
            mask = np.zeros(self.mother.ncv, dtype=bool)
        elif mode in ['intersection', 'intersect', 'and']:
            mask = np.ones(self.mother.ncv, dtype=bool)
 
        ##For CV
        if cvreg or cv:
            tmp = np.zeros(self.mother.ncv, dtype=bool)
            if isinstance(cvreg, str) and cvreg=='all':
                cvreg = []
                for v in self.mother.cvregions.values():
                    cvreg = extend(cvreg, v)
                cvreg = np.unique(cvreg)

            #import ipdb; ipdb.set_trace()
            for cvr in extend(cvreg):
                if isinstance(cvr, str):
                    cvr = extend(self.mother.cvregions[cvr])
                cvr = extend(cvr)
                if len(cvr) > 1:
                    itmp = np.zeros(self.mother.ncv, dtype=bool)
                    for i in extend(cvr):
                        itmp = np.logical_or(itmp, self.mother.cvreg == i)
                    tmp = np.logical_or(tmp, itmp)
                else:
                    tmp = np.logical_or(tmp, self.mother.cvreg == cvr)
            mask =  np.logical_or(mask, tmp)

        ##Create cv_and
        if cv_and:
            pass



        ## ATTENTION: Allow to use the names of xregions and yregions
        ## here directly as fcreg??? For some reason it is not working??
        ## Maybe because it returns a list??
        ## ATTENTION: FOR MASTU FC MASKS NOT WORKING??
        ##For FC
        if fcreg or fc:
            tmp  = np.zeros(self.mother.ncv, dtype=bool)
            fcregions = update(self.mother.xregions, self.mother.yregions)
            for fcr in extend(fcreg):
                if isinstance(fcr, str):
                    fcr = fcregions[fcr]
                #for ifc in np.where(self.mother.fcreg == fcr): ##works
                for ifc in np.where(self.mother.fcreg == fcr)[0]: ##BETTER?
                    icv = self.mother.ifccv[ifc]
                    tmp[icv] = 1
            mask =  np.logical_and(mask, tmp)

        ##For FT
        if ftreg or ft:
            tmp  = np.zeros(self.mother.ncv, dtype=bool)
            for ftr in extend(ftreg):
                if isinstance(ftr, str):
                    ftr = self.mother.ftregions[ftr]
                for ift in np.where(self.mother.ftreg == ftr)[0]:
                    icv = self.mother.iftcv[ift]
                    for i in icv: 
                        tmp[i] = 1
            mask =  np.logical_and(mask, tmp)

        ##For FS
        if fsreg or fs:
            tmp  = np.zeros(self.mother.ncv, dtype=bool)
            for fsr in extend(fsreg):
                if isinstance(fsr, str):
                    fsr = self.mother.fsregions[fsr]
                for ifs in np.where(self.mother.fsreg == fsr)[0]:
                    icv = self.mother.ifscv[ifs]
                    for i in icv: 
                        tmp[i] = 1
            mask =  np.logical_and(mask, tmp)


#        ###For labels too
#        ##FCLABEL (to identify cells in contact with structures)
#        ##For FC
#        ## Default fclabel is {'mcw':-43, etc,} but it can be overriden
#        ## by regions.yaml  or labels.yaml
#        tmp  = np.zeros(self.mother.ncv, dtype=bool)
#        for fcl in extend(fcreg):
#            if isinstance(fcl, str):
#                fcl = fcregions[fcr] ##TO BE DONE
#            for ifc in np.where(self.mother.fclbl == fcr):
#                icv = self.mother.ifccv[ifc]
#                tmp[icv] = 1
#        mask =  np.logical_or(mask, tmp)



        if real_cells:
            mask[self.mother.nci:] = 0 #Only real cells.
        if indices:
            mask = np.where(mask)[0]
        return mask






    ## ATTENTION: Under construction
#    def ifc(self, fcreg='all', fsreg='all', fc_and=None,
#        cvreg=[], ftreg=[], fcxreg=[], fcyreg=[],
#        fclbl=[], cv=[], fc=[], ft=[], fs=[],
#        fcx=[], fcy=[], **kwargs):
    def ifc(self, fc='all', fs='all', fc_and=None,
        cv=[], ft=[], fcx=[], fcy=[], fclbl=[],
        **kwargs):
        """
        """

        indices = priority(['inds', 'indices'], kwargs, False)
        real_cells = priority(['rlcl', 'real', 'real_cells'], kwargs, True)

        mode = priority(['mode', 'logic'], kwargs, 'combination')
        if mode in ['combination', 'union', 'combine', 'or']:
            mask = np.zeros(self.mother.nfc, dtype=bool)
        elif mode in ['intersection', 'intersect', 'and']:
            mask = np.ones(self.mother.nfc, dtype=bool)


    ## FACES: 
        fc = extend(fc)
        fcr = []
        fcnames = update(self.mother.xregions, self.mother.yregions)
        for ifcr in fc:
            if isinstance(ifcr, str) and ifcr=='all':
                fcr = []
                for k, v in fcnames.items():
                    fcr = extend(fcr, v)
                fcr = np.unique(fcr)
                break
            if isinstance(ifcr, str):
                fcr = extend(fcr, fcnames[ifcr])
            else:
                fcr = extend(fcr, ifcr)

        if any(fcr):
            import ipdb; ipdb.set_trace()
            tmp  = np.zeros(self.mother.nfc, dtype=bool)
            for ifcr in fcr:
                for ifc in np.where(self.mother.fcreg == ifcr)[0]:
                    ind = self.mother.ifccv[ifc]
                    tmp[ind] = 1
            mask =  np.logical_and(mask, tmp)


        ##For CV
        if cvreg or cv:
            tmp = np.zeros(self.mother.ncv, dtype=bool)
            if isinstance(cvreg, str) and cvreg=='all':
                cvreg = []
                for v in self.mother.cvregions.values():
                    cvreg = extend(cvreg, v)
                cvreg = np.unique(cvreg)

            #import ipdb; ipdb.set_trace()
            for cvr in extend(cvreg):
                if isinstance(cvr, str):
                    cvr = extend(self.mother.cvregions[cvr])
                cvr = extend(cvr)
                if len(cvr) > 1:
                    itmp = np.zeros(self.mother.ncv, dtype=bool)
                    for i in extend(cvr):
                        itmp = np.logical_or(itmp, self.mother.cvreg == i)
                    tmp = np.logical_or(tmp, itmp)
                else:
                    tmp = np.logical_or(tmp, self.mother.cvreg == cvr)
            mask =  np.logical_or(mask, tmp)


        if real_cells:
            mask[self.mother.nci:] = 0 #Only real cells.
        if indices:
            mask = np.where(mask)[0]
        return mask





## ClassicalMask should have ixy






###### ========================================================================

class StructuredMasks(BaseMasks):
    ##ATTENTION: Call it ixy, to make sure that one understands?
    #def icv(self, cvreg=[], fcreg=[], ix=[], iy=[], **kwargs):
    def ixy(self, cvreg=[], fcreg=[], ix=[], iy=[], **kwargs):
        """ 
        ix, iy, same idea as with regions in the past:
        Mode determines whether returns ix = [...] and iy = [...] 
        returns union of rings or intersection.
        """
        self.log.exception("Not implemenyed yet.")


    @solps_property
    def gdcl(self):
        """(nx, ny): Guard cells = True."""
        self._gdcl = np.zeros((self.mother.nx,self.mother.ny), dtype=bool)

        self._gdcl[0] = 1
        self._gdcl[-1] = 1
        self._gdcl[:,0] = 1
        self._gdcl[:,-1] = 1

    @property
    def guard_cells(self):
        """Alias for :py:attr:`~gdcl`"""
        return self.gdcl

    @solps_property
    def rlcl(self):
        """(nx, ny): Real cells = True."""
        self._rlcl = np.logical_not(self.gdcl)

    @property
    def real_cells(self):
        """Alias for :py:attr:`~rlcl`"""
        return self.rlcl
#
#
#    def regions(self, regions, mode='combination', rlcl=True):
#        """
#
#        Parameters
#        ----------
#
#        regions: {'v', 'x', 'y'} dict
#            Type of region: Volume, x-directed, or y-directed.
#            The associated list contains the indices to be found in said
#            type of region.
#            Important: index of region is not Python index (starts at 1).
#
#        mode: {'combination', 'intersection'}, optional
#            Combination or intersection of all given the regions and indices.
#
#        rlcl: bool, optional
#            Whether or not only real cells should be considered.
#
#        Returns
#        -------
#        (nx, ny)
#            Boolean array with True wherever mode applies to the given regions.
#        """
#
#        str_to_reg = {'v':0, 'x':1, 'y':2}
#
#        if mode == 'combination':
#            mask = np.zeros((self.mother.nx, self.mother.ny))
#        elif mode == 'intersection':
#            mask = np.ones((self.mother.nx, self.mother.ny))
#
#        for d, inds in list(regions.items()):
#            if isinstance(inds,int) or isinstance(inds,float):
#                inds = [int(inds)]
#            if isinstance(d,str):
#                d = str_to_reg[d]
#            for ind in inds:
#                tmp = self.mother.region[...,d] == ind
#                if mode == 'combination':
#                    mask = np.logical_or(mask, tmp)
#                if mode == 'intersection':
#                    mask = np.logical_and(mask, tmp)
#
#        if rlcl:
#            return np.logical_and(mask,self.rlcl)
#        else:
#            return mask
#
#
#    #Core, SOL, inndiv and outdiv to be defined in subclasses.
#
#    @solps_property
#    def notcore(self):
#        """(nx, ny): All real cells except the core = True.
#        """
#        self._notcore = self.rlcl*(1-self.core)
#
#
#    #@solps_property
#    #def pfr(self):
#    #    self._pfr = (1-self.sol) * self.notcore * self.rlcl
#
#    @solps_property
#    def pfr(self):
#        """(nx, ny): Real cells in the private flux region.
#        """
#        self._pfr = np.logical_and(self.real_cells,
#                np.logical_and(self.notcore,
#                    np.logical_not(self.sol)))
#
#
#    @solps_property
#    def div(self):
#        """(nx, ny): Real cells in the main inner + 0uter divertors.
#        """
#        self._div = self.inndiv + self.outdiv
#
#    @property
#    def divertor(self):
#        """Alias of :py:attr:`~div`"""
#        return self.div
#
#    @solps_property
#    def notdiv(self):
#        """(nx, ny): Every real cell  except inner+outer divertors.
#        """
#        self._notdiv = self.rlcl*(
#                np.ones((self.mother.nx,self.mother.ny))-self.div)
#
#
#    @solps_property
#    def innertarget(self):
#        """(nx, ny): Real cells along the inner diveror are True.
#        """
#        self._innertarget = np.zeros((self.mother.nx,self.mother.ny))
#        self._innertarget[self.mother.iinn,:] = 1
#
#    @solps_property
#    def outertarget(self):
#        """(nx, ny): Real cells along the outer diveror are True.
#        """
#        self._outertarget = np.zeros((self.mother.nx,self.mother.ny))
#        self._outertarget[self.mother.iout,:] = 1
#
#
#
#    def generate(self, **kwargs):
#        """ Pluggin for tools.core.generate."""
#        return generate_mask(self.mother, **kwargs)
#
#    def __call__(self, **kwargs):
#        return self.generate(**kwargs)
#
#
#    ## ATTENTION 2021: Is logic correctly applied??
#    @property
#    @try_block
#    def xpoint(self):
#        """Grid mask for the grid cells surrounding the x-point."""
#        self._xpoint = np.zeros((self.mother.nx,self.mother.ny))
#        for nx in range(self.mother.nx):
#            for ny in range(self.mother.ny):
#                cell = self.mother.grid[nx,ny] # [nvertex,R|z]
#                for v in range(len(cell)):
#                    if (cell[v][0] == self.mother.xpoint[0] and
#                        cell[v][1] == self.mother.xpoint[1]):
#                        self._xpoint[nx,ny] = 1
#
#
#
#
#
#
#
########### ====================================================================
#
#
#class MasksLSN(BaseMasks):
#
#    def __init__(self,mother):
#        super().__init__(mother)
#
#    ## Region definitions
#    @solps_property
#    def core(self):
#        """ (nx, ny): Real cells in the core region = True."""
#        self._core = self.regions({'v':1}, rlcl=self.irlcl)
#
#    ## DEFINITION CHANGED: Before it was SOL including in divs.
#    ## Now only SOL above divs. This one is better??
#    ## Maybe call it main_sol?
#    @solps_property
#    def mainsol(self):
#        """ (nx, ny): Real cells in the main (outside of div) SOL region = True."""
#        self._mainsol = self.regions({'v':2}, rlcl=self.irlcl)
#
#    @solps_property
#    def sol(self):
#        """ (nx, ny): Real cells in the SOL region = True."""
#        self._sol = np.logical_and(self.real_cells,
#                self.generate(yrange=[self.mother.sep, self.mother.ny]))
#
#    @solps_property
#    def outdiv(self):
#        """ (nx, ny): Real cells in the outer divertor region = True."""
#        self._outdiv = self.regions({'v':4}, rlcl=self.irlcl)
#
#    @solps_property
#    def inndiv(self):
#        """ (nx,ny): Real cells inner divertor region = True."""
#        self._inndiv = self.regions({'v':3}, rlcl=self.irlcl)
#
#
#
#
#
#
#
#
########### ====================================================================
#
#
#class MasksUSN(BaseMasks):
#
#    def __init__(self,mother):
#        super().__init__(mother)
#
#
#    ## Region definitions
#    @solps_property
#    def core(self):
#        """ (nx,ny): Core region."""
#        self._core = self.regions({'v':1}, rlcl=self.irlcl)
#
#    @solps_property
#    def sol(self):
#        """ (nx,ny): SOL region."""
#        self._sol = self.regions({'v':2}, rlcl=self.irlcl)
#
#    @solps_property
#    def outdiv(self):
#        """ (nx,ny): Outer divertor region."""
#        #TO BE TESTED
#        self._outdiv = self.regions({'v':3}, rlcl=self.irlcl)
#
#    @solps_property
#    def inndiv(self):
#        """ (nx,ny): Inner divertor region."""
#        #TO BE TESTED
#        self._inndiv = self.regions({'v':4}, rlcl=self.irlcl)
#
#
#
#
#
#
#
#
#
#
#
#
########### ====================================================================
#
#
#class MasksDDNU(BaseMasks):
#    """Class containing different grid masks."""
#
#    def __init__(self,mother):
#        super().__init__(mother)
#
#    @solps_property
#    def gdcl(self):
#        """(nx,ny): Guard cells."""
#        self._gdcl = np.zeros((self.mother.nx,self.mother.ny))
#
#        self._gdcl[0] = 1
#        self._gdcl[-1] = 1
#        self._gdcl[:,0] = 1
#        self._gdcl[:,-1] = 1
#        self._gdcl[self.mother.iout-1,:] = 1
#        self._gdcl[self.mother.iinn+1,:] = 1
#
#
#
#    ## Region definitions
#    # Core
#    @solps_property
#    def inncore(self):
#        """ (nx,ny): Inner core region."""
#        self._inncore = self.regions({'v':1}, rlcl=self.irlcl)
#
#    @solps_property
#    def outcore(self):
#        """ (nx,ny): Outer core region."""
#        self._outcore = self.regions({'v':5}, rlcl=self.irlcl)
#
#    @solps_property
#    def core(self):
#        self._core = np.logical_or(self.outcore, self.inncore)
#
#
#    ## ATTENTION: These are still not the complete SOLs.
#    # Sol
#    @solps_property
#    def outsol(self):
#        """ (nx,ny): Outer (complete) SOL region."""
#        self._outsol = self.regions({'v':6}, rlcl=self.irlcl)
#
#    @solps_property
#    def innsol(self):
#        """ (nx,ny): Inner (complete) SOL region."""
#        self._innsol = self.regions({'v':2}, rlcl=self.irlcl)
#
#    @solps_property
#    def sol(self):
#        """ (nx,ny): Complete SOL region."""
#        self._sol = np.logical_or(self.outsol, self.innsol)
#
#    #Create sol1 (only main), sol2(only secondary)
#
#
#
#    # Divertor
#    @solps_property
#    def outdiv(self):
#        """ (nx,ny): Main outer divertor region."""
#        self._outdiv = self.regions({'v':7}, rlcl=self.irlcl)
#
#    @solps_property
#    def inndiv(self):
#        """ (nx,ny): Main inner divertor region."""
#        self._inndiv = self.regions({'v':4}, rlcl=self.irlcl)
#
#
#    # Secondary Divertor
#    @solps_property
#    def outdiv2(self):
#        """ (nx,ny): Secondary outer divertor region."""
#        self._outdiv2 = self.regions({'v':8}, rlcl=self.irlcl)
#
#    @solps_property
#    def inndiv2(self):
#        """ (nx,ny): Secondary inner divertor region."""
#        self._inndiv2 = self.regions({'v':3}, rlcl=self.irlcl)
#
#    @solps_property
#    def div2(self):
#        """ Secondary Inner + Outer divertors.
#        Guard cells are excluded.
#        """
#        self._div2 = self.inndiv2 + self.outdiv2
#
#    @property
#    def divertor2(self):
#        """Alias of self.div"""
#        return self.div2
#
#    @solps_property
#    def notdiv2(self):
#        """ Everything except inner+outer divertors.
#        Guard cells are excluded.
#        """
#        self._notdiv2 = self.rlcl*(
#                np.ones((self.mother.nx,self.mother.ny))-self.div2)
#
#
#    @solps_property
#    def innertarget2(self):
#        self._innertarget2 = np.zeros((self.mother.nx,self.mother.ny))
#        self._innertarget2[self.mother.iinn2,:] = 1 # nx=1 (rlcl)
#
#    @solps_property
#    def outertarget2(self):
#        self._outertarget2 = np.zeros((self.mother.nx,self.mother.ny))
#        self._outertarget2[self.mother.iout2,:] = 1
#
#
#
#
#    @solps_property
#    def hfs(self):
#        """ High field side region of grid.
#        """
#        self._hfs = np.logical_and(self.real_cells,
#                self.regions({'v':[1,2,3,4]}))
#                    
#    @solps_property
#    def lfs(self):
#        """ Low field side region of grid.
#        """
#        self._lfs = np.logical_and(self.real_cells,
#                self.regions({'v':[5,6,7,8]}))
#                    
#
#
#    ## LOGIC IS NOT CORRECTLY APPLIED
#    @property
#    @try_block
#    def xpoint(self):
#        """Grid mask for the grid cells surrounding the x-point."""
#        self._xpoint = np.zeros((self.mother.nx,self.mother.ny))
#        for nx in range(self.mother.nx):
#            for ny in range(self.mother.ny):
#                cell = self.mother.grid[nx,ny] # [nvertex,R|z]
#                for v in range(len(cell)):
#                    if (cell[v][0] == self.mother.xpoint[0] and
#                        cell[v][1] == self.mother.xpoint[1]):
#                        self._xpoint[nx,ny] = 1
#
#
#
#
#
#
#
########### ====================================================================
#
#class MasksCDN(MasksDDNU):
#    def __init__(self, mother):
#        super().__init__(mother)
#
#    @solps_property
#    def gdcl(self):
#        """(nx,ny): Guard cells."""
#        self._gdcl = np.zeros((self.mother.nx,self.mother.ny))
#
#        self._gdcl[self.mother.iout+1] = 1
#        self._gdcl[self.mother.iinn-1] = 1
#        self._gdcl[self.mother.iout2-1,:] = 1
#        self._gdcl[self.mother.iinn2+1,:] = 1
#        self._gdcl[:,0] = 1
#        self._gdcl[:,-1] = 1
#
#
#    #Core, sol, inndiv and outdiv 'v' regions are the same as DDNU.
#    # The only change is what is considered main
#
#    # Divertor
#    @solps_property
#    def outdiv(self):
#        """ (nx,ny): Lower outer divertor region."""
#        self._outdiv = self.regions({'v':8}, rlcl=self.irlcl)
#
#    @solps_property
#    def inndiv(self):
#        """ (nx,ny): Lower inner divertor region."""
#        self._inndiv = self.regions({'v':3}, rlcl=self.irlcl)
#
#
#    # Secondary Divertor
#    @solps_property
#    def outdiv2(self):
#        """ (nx,ny): Secondary outer divertor region."""
#        self._outdiv2 = self.regions({'v':7}, rlcl=self.irlcl)
#
#    @solps_property
#    def inndiv2(self):
#        """ (nx,ny): Secondary inner divertor region."""
#        self._inndiv2 = self.regions({'v':4}, rlcl=self.irlcl)
