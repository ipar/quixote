import numpy as np
import dataclasses as dtc

from collections import OrderedDict

from ..tools import parse_adasf15

@dtc.dataclass
class SpectralLines:
    wl: float
    exc: float
    rec: float
    tot: float = dtc.field(init=False)
    name: str
    def __post_init__(self):
        self.tot = self.exc+self.rec


class Spectroscopy:
    def __init__(self, mother):
        self.mother = mother
        self.log = mother.log
        self.log.info('Loading Spectroscopy()')
        self.log.info('Attention!')
        self.log.info(' Calculation of photon emissivities can take long.')

    # =========================================================================
    @solps_property
    def pec(self):
        """
        """

    @solps_property
    def emission(self):
        """
        """

    @solps_property
    def power(self):
        """
        """

    @solps_property
    def total_emission(self):
        """
        """

    @solps_property
    def total_power(self):
        """
        """

    @solps_property
    def lz_adas(self):
        """
        """

    # =========================================================================
    
    def get_line(self, species, wl, maxdiff=0.1):
        """ Can be done
        """

    def highest_emission(self, **kwargs):
        """ I think can be done
        """

    def plot_spectrum(self, **kwargs):
        """
        """

    def plot_lz(self, **kwargs):
        return lz_plot(self, **kwargs)

    
    # =========================================================================

    
    def _calculate_adas_emissivity(self, si):
        """
        I think only needs parse_adasf15.
        """

        emissivity = OrderedDict()

        if self.mother.za[si] == self.mother.zn[si]:
            return OrderedDict()




    def _get_backup_filename(self):
        """ Does not need anything strange.
        """

    def _store_data(self):
        """ Does not need anything strange.
        """

    def _restore_data(self):
        """ Does not need anything strange.
        """

    def _calculate_lz(self, atom, **kwargs):
        """
        Can be done in SolpsData
        """

    ##ATTENTION: This one needs the run_adas
    def _calculate_lz_adas(self, atom, te, ne):
        """ Without really investigating this, it seems that 
        this is probably a way to interpolate pec for a given ne and te,
        and calculate stuff out of that pec.
        """








# =============================================================================
class lz_plot:
    def __init__(self, spectroscopy, **kwargs):
        """
        """

    def _guess_species(self):
        """
        """

    def _get_lz(self):
        """
        """

    def _cett(self, val):
        """
        """

    def _plot(self):
        """
        """


