{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ad9c2531",
   "metadata": {},
   "source": [
    "# Quickstart"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd760739",
   "metadata": {},
   "source": [
    "## Quickstart with the SolpsData class"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "625d733e",
   "metadata": {},
   "source": [
    "The main object of ``solpspy`` is the `SolpsData` class.  \n",
    "To obtain a valid `SolpsData` instance, one needs either an MDS+ shot number or the path to SOLPS run directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "326f466c",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "RundirDataUSN(Basic Example) -- WARNING: natm, nmol and nion could not be retrieved.\n",
      "RundirDataUSN(Basic Example) -- WARNING: Assuming b2 standalone run.\n",
      "RundirDataUSN(Basic Example) -- WARNING: rtnt, rtnn and rtns could not be retrieved\n"
     ]
    }
   ],
   "source": [
    "import quixote\n",
    "\n",
    "#We will use one of the mockup test cases as an example.\n",
    "data = quixote.SolpsData('examples/structured', name='Basic Example')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "e9261774",
   "metadata": {},
   "outputs": [],
   "source": [
    "#%matplotlib inline\n",
    "#import os\n",
    "#import numpy as np\n",
    "#import matplotlib.pyplot as plt\n",
    "\n",
    "#import quixote\n",
    "#import quixote.tools as tools\n",
    "#tools.create_log('root', level='critical')\n",
    "\n",
    "#We will use one of the mockup test cases as an example.\n",
    "#data = quixote.SolpsData('examples/structured')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35eed5e1",
   "metadata": {},
   "source": [
    "This shot object contains all the information that you may want out of your SOLPS simulation.  \n",
    "You can access different variables by just calling them:  \n",
    "> Variables in solpspy are, in general, lazy. Therefore, data are generally only loaded when they are needed, which is good for loading times and memory consumption."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "b3f0e87a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 0.07948441,  0.05107454,  0.06084578, ..., 13.85990939,\n",
       "        13.19605742, 22.06068771],\n",
       "       [ 0.11971366,  0.12293434,  0.15152932, ..., 15.02907119,\n",
       "        14.42967845, 21.84534332],\n",
       "       [ 0.17691242,  0.18163479,  0.23103828, ..., 17.15156738,\n",
       "        16.5362884 , 21.81155498],\n",
       "       ...,\n",
       "       [ 0.81640579,  0.83463112,  0.93028441, ..., 12.48210439,\n",
       "        11.82349228, 11.86190659],\n",
       "       [ 0.61299017,  0.62659933,  0.69399896, ..., 11.12430329,\n",
       "        10.7975912 , 11.44990882],\n",
       "       [ 0.43477613,  0.26301737,  0.2969481 , ..., 10.84758669,\n",
       "        10.54180455, 11.4603454 ]])"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data.te"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "a1724c55",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(98, 38)"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data.te.shape # (nx, ny)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "a2fdb2b4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "({GRID_DIMS}): Electron temperature [eV]\n"
     ]
    }
   ],
   "source": [
    "data.help(\"te\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "866f15ea",
   "metadata": {},
   "source": [
    "This shows that the property te is a (nx, ny) numpy array which contains the electron temperature in eV.\n",
    "Other quantities have other shapes, and one should always know what one is doing when manipulating indices, just like in any general numpy array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "3a8ec223",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([1018.75343592,  959.42569076,  846.65840452,  743.87502289,\n",
       "        650.80965986,  567.23185143,  492.68434098,  426.52888048,\n",
       "        368.82707439,  319.29990917,  277.07280346,  241.45830647,\n",
       "        211.97689592,  188.4214563 ,  170.09632058,  155.96981942,\n",
       "        145.07217339,  136.39140238,  129.01288722,  124.77251199,\n",
       "        122.32512482,  118.91813771,  114.80167244,  110.19827931,\n",
       "        105.30759566,  100.28857033,   95.24052747,   90.20259212,\n",
       "         85.1666918 ,   80.16855296,   75.37053448,   70.83378762,\n",
       "         66.50859766,   62.37903302,   58.44141316,   54.78565536,\n",
       "         51.72522375,   50.9283933 ])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data.te[data.iomp] # This is a radial profile at the outer midplane (omp)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "a300566e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(38,)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data.te[data.iomp].shape # It has a size of ny, as expected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "8f3c92c6",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([  0.22752144,   0.61970506,   0.99288634,   1.12390458,\n",
       "         1.21741923,   1.30527105,   1.41000091,   1.53157668,\n",
       "         1.66349149,   1.81597506,   2.01546176,   2.25810122,\n",
       "         2.60943689,   3.1420858 ,   3.89353444,   5.11971149,\n",
       "         6.90525494,   9.40741078,  12.49747175,  15.95225871,\n",
       "        19.77434115,  24.61682224,  31.49642   ,  43.81305317,\n",
       "        69.87323972,  99.1528499 , 109.7790798 , 115.35728818,\n",
       "       119.21280995, 121.79281267, 123.52807984, 124.74220066,\n",
       "       125.62765006, 126.31376458, 126.84867381, 127.27594544,\n",
       "       127.62161467, 127.90474604, 128.13981856, 128.33637905,\n",
       "       128.5007541 , 128.63547582, 128.74080515, 128.81758652,\n",
       "       128.86824619, 128.89445485, 128.89479143, 128.86455668,\n",
       "       128.79383951, 128.66477502, 128.45293863, 128.13920774,\n",
       "       127.7440401 , 127.33236773, 126.9315511 , 126.52943195,\n",
       "       126.11401244, 125.68562852, 125.2418466 , 124.77251199,\n",
       "       124.2714716 , 123.73822448, 123.16775249, 122.54636865,\n",
       "       121.85090839, 121.04571524, 120.07964774, 118.85681614,\n",
       "       117.26523355, 115.07271599, 111.9455498 , 107.52864418,\n",
       "        99.86003668,  64.2540745 ,  50.18437127,  44.15756191,\n",
       "        40.10656313,  37.07836583,  34.59563976,  32.44687464,\n",
       "        30.47463499,  28.61656827,  26.83617456,  25.09585957,\n",
       "        23.35536478,  21.61317695,  19.85292714,  18.05250887,\n",
       "        16.23383673,  14.3673948 ,  12.47497084,  10.59748332,\n",
       "         8.7990806 ,   7.13095456,   5.504589  ,   3.42404292,\n",
       "         1.78469209,   0.60681682])"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data.te[:, data.isep] # This is a poloidal profile along the first flux surface in the SOL (sep)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "e9df5154",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(98,)"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data.te[:, data.isep].shape  # It has a size of nx, as expected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "511a73d6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "usn\n"
     ]
    }
   ],
   "source": [
    "print(data.magnetic_geometry) # This case is an upper single null."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "71646e0c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "solps-iter\n",
      "03.000.008\n"
     ]
    }
   ],
   "source": [
    "print(data.solps_code) # Version of SOLPS which figures on the header of files.\n",
    "print(data.solpsversion) # Release version of the code on the header of files.\n",
    "#Annoyingy, solps_version would return solps-iter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a14ea284",
   "metadata": {},
   "outputs": [],
   "source": [
    "#data.outmp.te.shape #This is the time trace of the electron temperature at the outer midplane (ntime, ny)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75f20871",
   "metadata": {},
   "source": [
    "<br/>\n",
    "<br/>\n",
    "\n",
    "## Structure of the SolpsData class"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8d5ff46",
   "metadata": {},
   "source": [
    "Indices (int):  \n",
    "- sep: Radial index of the first flux tube in the main SOL.\n",
    "- sep2: Radial index of the first flux tube in the secondary SOL, if it exists.\n",
    "- omp / imp: Poloidal index of the outer / inner midplane.\n",
    "- iout / iinn: Poloidal index of the main outer / inner target.\n",
    "- iout2 / iinn2: Poloidal index of the secondary outer / inner target, if it exists.\n",
    "\n",
    "Basic variables:\n",
    "- te / ti: electron / ion temperature.\n",
    "- ne: electron density.\n",
    "- na: density of each fluid species. This includes the fluid neutrals.\n",
    "- ua: parallel velocity of each fluid species. \n",
    "- dab2: EIRENE atomic density in the b2 grid.\n",
    "- dmb2: EIRENE molecular density in the b2grid.\n",
    "\n",
    "Time traces:\n",
    "- outmp / innmp: Name fields containing the time traces corresponding to the outer / inner midplanes.\n",
    "- out / nn: Same but for outer / inner main target.\n",
    "- out2 / inn2: Same but for outer / inner secondary target.\n",
    "\n",
    "Variables in time traces: The available quantities may differ depending on the type (midplane, target or region), but some are common: te, ti, ne, na, ..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8c84db89",
   "metadata": {},
   "source": [
    "### Hierarchy of files"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7bc9f9a",
   "metadata": {},
   "source": [
    "Some data are present in more than one file. For example, te could be read from b2fplasmf, b2fstate or b2fstati.  \n",
    "The specific hierarchy of files can be found and modified in the configuration files.  \n",
    "In general: b2fplasmf > b2fgmtry > b2fstate > b2fstati."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
