.. _help:

Help and information
====================

In an interactive session, finding information on the definition of a property
or the signature of a function can be very useful.

Help can be found with the ``help`` function or the standard Python ``?``,
although the last does not always correctly accounts for Python properties.
At the moment the ``help`` function only works one level deep, and in the new
versions of Python ``?`` seems to work much better in general, so find which
function works best for your setting.

.. jupyter-execute::
   :hide-code:
   :hide-output:

    import quixote.tools as tools
    tools.create_log('root', level='critical')

.. jupyter-execute::

    import quixote
    data = quixote.SolpsData('guides/examples/structured')

.. jupyter-execute::
    
    data.help('te')

.. jupyter-execute::
    
    # Even if it does not produce an output here
    # it works in IPython, Jupyter notebooks, etc.
    data.te?


A more advance alternative consist in checking the code itself.
Due to the inheritance model, quantities and functions may be defined multiple
times in different classes. In order to know which class defines the version of
the field that you are using, you can use the ``whichclass`` function.

.. jupyter-execute::
    
   quixote.whichclass(data, 'te') 

.. jupyter-execute::
    
   quixote.whichclass(data, 'fnay') 


Using this information, one can find the definition of the field and
its implementation.
