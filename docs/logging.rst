.. _logging:

Logging
=======

``Quixote`` use sensible logging techniques to adapt to the different
needs and preferences of the users.

``print()`` is banned. Instead the standard library's ``logging`` module is used,
and, in very rare situations, the ``warning`` module.


``logging`` allows the user to easily setup and configure the level of the messages
(debug, info, error, critical) that they want to receive and the output for 
that information (the console, a log file, etc).


The only particularity of ``logging`` in ``quixote`` is that ability to pass
a name or identifier to a solps object for the logging related to that object.
Otherwise, ``quixote`` resorts to either the run directory's name for Rundir cases
or the shot number for MDS+ cases.


For everything else, the common usage of ``logging`` should be undisturbed.

