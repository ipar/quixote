.. _overview:

Overview
========

SolpsData class
---------------

The main object of ``quixote`` is the ``SolpsData`` class.
To obtain a valid ``SolpsData`` instance, one needs either an MDS+ shot
number or the path to SOLPS run directory.


.. jupyter-execute::
   :hide-code:
   :hide-output:

    import os
    import numpy as np
    import matplotlib.pyplot as plt

    import copy
    import quixote.tools as tools
    tools.create_log('root', level='critical')

.. jupyter-execute::

    import quixote
    data = quixote.SolpsData('guides/examples/structured')


This object contains all the information that you may want out of
your SOLPS simulation.

.. jupyter-execute::
    
    print(data.te.shape)
    print(data.te)



.. note::
    
    Variables in ``quixote`` are, in general, lazy.

    Therefore, data are generally only loaded into memory when they are
    needed, which is good for both loading times and memory usage.


For information on how to get help and details about quantities' definitions,
function signatures, etc., see :ref:`help`.


Just like in aviation, the priority in this quick guide will be the following:
Aviate, Navigate, Communicate.


Aviate
------

The quality of tool is irrelevant if you cannot use it.

For most users, installing ``quixote`` should be pretty simple:


    #. Clone `quixote <https://gitlab.com/ipar/quixote>`_ into a location NOT in ``PYTHONPATH`` (to avoid module discovery conflicts).

    #. Run ``pip install -e .`` inside ``quixote``.


For further installation details, see :ref:`installation`.

It is also recommended to clone the testing suite of ``quixote``, 
`quixote-test <https://gitlab.com/ipar/quixote-test>`_ and follow
the :ref:`testing` guide.



Navigate
--------

Grids are strange places and if you do not navigate correctly, you will get lost.

Indices
^^^^^^^

The first way in which one can orient oneself, and therefore navigate, in a grid is with the use of indices.


A myriad of ways to map and manipulate our grids are explained in :ref:`indices`, and I would highly recommend that users read the whole section before trying to reinvent the wheel.


For example, in an structured grid, the poloidal index of the OMP is:

.. jupyter-execute::

    data.iomp # So close!

So the following represents the radial electron temperature profile at the OMP:

.. jupyter-execute::

    data.te[data.iomp]


In ``quixote``, all indices and quantities relating different types
of indices start with **i**.

For an structured grid, the most commonly used indices are:

    * ``isep``: Radial index of the separatrix (first ring in the SOL).
    * ``iomp``, ``iimp``: Outer and inner midplanes.
    * ``iout``, ``iinn``: Outer and inner MAIN targets.
    * ``iout2``, ``iinn2``: Outer and inner secondary targets.
    * ``ioxp``, ``iixp``: Outer and inner sides of the main x-point or divertor entrance.
    * ``ioxp2``, ``iixp2``: Outer and inner sides of the secondary x-point or divertor entrance.
    
``quixote`` takes care about deciding what main and secondary mean.
That allows to effortlessly compare simulations with different magnetic geometries, i.e., not needing to consider whether the outer target is Target 1 or Target 4 in an USN as opposed to a LSN simulation.


The best way to navigate unstructured grids is still not established, so a myriad of different methods are provided.


A multitude of ways to dice and slice your grids are explained in :ref:`indices`, and I would highly recommend that users read the whole section before trying to reinvent the wheel.



Chords and LOS
^^^^^^^^^^^^^^

An alternative way to analyze the results of a SOLPS simulation pertains the
evaluation of quantities along chords or Lines of Sight, not dissimilar to many
experimental diagnostics.

This method has the main advantage of allowing the comparison of grids with 
very different dimensionality and structures, e.g. a structured grid vs. a TIARA grid which fills the whole vessel.

It is also very useful for comparison with experimental diagnostics which
rely on LOS.

LOS can be defined by the user on the fly during an interactive session or
they can be provided by a file ``los.coords`` in either the run directory
or the baserun.

For more information on everything related to chords, see :ref:`chords`.



Databases
^^^^^^^^^

Navigating does not only pertain the small scale orientation, but also
the project level organization.

A ``quixote`` database is a good way to catalogue and organize multiple
simulations, especially those sharing many characteristics. See :ref:`databases`.


There are two main ``quixote`` classes to exploit those databases:

    * ``Family``, see :ref:`family`, is a general collection of simulations which you want to analyze simultaneously, e.g. a power and density scan.

    * ``Uncertainty``, see :ref:`uncertainty`, is a collection of simulations, some of which are intimatelly related to each other called groups. These groups can be seeing as a 'main simulation' and its 'error bars'. For example, a collection of simulations based on different experimental plasmas, each of which is modelled by multiple simulations with sensibly different input parameters.




Communicate
-----------

Finally, it is time to present the results of these simulations and communicate
the news with the rest of the community.

``quixote`` does not aim to replace your own plotting scripts, but to provide
tools to make those scripts more robust and simpler.

Some plotting classes are provided by ``quixote`` for the more cumbersome tasks, such as plotting 2D data in a grid with the vessel.  
These classes, however, are designed to manipulate existing axis, so that they nicely interact with your existing plot structure, not only allowing but encouraging composition of multiple different plot types.

See :ref:`/guides/plotting.ipynb` for an overview of those plotting classes.

For a multitude of examples of different plots using ``quixote`` resources, see :ref:`gallery`.


``quixote`` also provides a Python alternative to already existing MATLAB scripts, such as the balance scripts, see :ref:`balance` (Not yet implemented)




``quixote`` as a building block
-------------------------------

``quixote`` can be used as the foundation or a component of more especialized codes.

The output of ``quixote`` is carefully streamed out via the Python
library ``logging``.
For details on how to use it for your advantage, see :ref:`logging`.
