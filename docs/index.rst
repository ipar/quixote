Quixote: SOLPS post-processing accesible to all!
================================================

.. image:: images/don_quixote.jpg
    :align: center
    :alt: Don Quixote by Pablo Picasso

Quixote is a Python 3 package which aims to ease post-processing of SOLPS
simulations for all type of people, from the beginner to the expert.

We aim to make Quixote the 'numpy of SOLPS'.

The source code of ``quixote``: https://gitlab.com/ipar/quixote

See :ref:`overview` to start immediately.



**Zen of** ``quixote``:

  * Do not reinvent the wheel.

  * Standard is better than non-standard.

  * General is better than specific.

  * Specific is better than non-working.

  * Focus on the physics of the simulations, not on how to handle them.


For that, `SolpsData` is a class that will take care of reading the files
for you, so that you have fast and standardized access to the input and
output data of any SOLPS run.



.. note::

    ``quixote`` aims to ease the exploitation of SOLPS simulations by the
    general users and non-experts.  
    It **does not** intend to ease the usage of Python.
    
    I.e.: **Learn your Python** to realize the full potential of ``quixote``.  
    If you do not have a solid grasp of the basic Python syntax, you won't
    be able to even know what is general Python and what is ``quixote`` specific.


.. toctree::
    :maxdepth: 2
    :caption: Contents:
    
    overview

    installation

    indices
    unstructured
    databases

    guides/plotting
    gallery
    
    logging
    help

    authors



..
    Indices and tables
    ==================
    
    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
