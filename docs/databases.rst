.. _databases:

Working with databases
======================

The 5 seconds API rundown is shown in the following code.
The result is an object (db in the example), which contains the parsed
databases following the criteria set in ``include`` and ``exclude``,
and which entries are ordered arcording to ``order``.

.. jupyter-execute::
   :hide-code:

    import os
    import numpy as np
    import matplotlib.pyplot as plt
    import itertools
    from collections import OrderedDict
    import copy

.. jupyter-execute::

    import quixote




One can use this ``Family`` object to manipulate, plot, sample, etc multiple
simulations at the same time.

For Jupyter notebook examples about what to do with
working ``Family``, c.f. 

To account for uncertainty using multiple simulations, ``Uncertainty`` eases
the task of manipulating that type of layered database.


.. _family:

``Family`` is important
-----------------------

Ohana means ``Family``, and ``Family`` means that no simulation gets left behind
or forgotten, unless specified.

A Family class will load all the databases (DB) which are either directly privided
or pointed by the provided indices, and it will filter all those entries according
to the given criteria.

The Family class API can be found in: :class:`classes.family.Family`.


Indices
-------

Indices are a collection of DB with information of what experiments of which
DB should be incorporated in the Family collection. The higher in the list,
the higher the priority, i.e., if two experiments share the same name,
the one of the highest DB will be used.

The index file is a YAML file containing a list of database names which
might be dictionaries of the desired experiments (otherwise all the
experiments in the DB are available). 

As an example, the content of ``index_dummy.yaml`` could be::

    # All the experiments in database.yaml will be available.
    - database.yaml

    # Only Experiment1 and Experiment2 will be available.
    # from another_db.yaml. These might be replaced by
    # experiments with the same name in database.yaml.
    - subdirectory1/another_db.yaml:
        - Experiment1
        - Experiment2

These are paths to the yaml files that will then be parsed. One could have a single index with multiple DB located in different directories.


Creating a YAML DB
------------------
A YAML DB suitable for the Family class contains a number of experiments, with a description, a set of default labels and a set of cases with their specifications.


The basic structure a YAML database is the following::

    'Name of experiment':
        description: "A nice description"

        defaults:
            id: ...
            rootpath: ...
            basepath: ...
            path: ...
            [Any number of labels]

        cases:

            - [Any
              number of
              labels]

            - [Any
              number of
              labels]

              ...


The experiments are identified by a given name, which will be used to select such experiments.
The default values are intended to work in a similar fasion to baserun. If nothing in a given case specification overwrites the default value, this is asigned to the dict of the case.
These labels and parameters will be the means by which Family will include, exclude and order your cases.

There are two types of mandatory labels that must be present in each case (either via defaults or specified case by case):

    * **id**: The identifier of the case. Usually this is either an path to the run directory or an MDS shot number. In theory it can be any number or string, but **warning**, simulations are loaded by id by default.

    * **rootpath, basepath, path**: These three parameters are used to construct abspath. Following the standard SOLPS rundir structure, rooth path would be the path to the root of the experiments, basepath would be the relative path to the specific experiment and path would be the relative path to the rundirectory.

.. note::

    With the standard SOLPS structure, rootpath and basepath should go into the defaults of the experiment and path into the cases dictionaries. If the run directories are contained in different experiments, then roopath would go in default and both basepath and path into the case dictionary.


.. note::

   Solpspy will try to stich together roopath+basepath+path, in that order. However, one or more pieces are allowed to be absent. E.g., one could asign the entire rundir path to path.


Cross referencing
^^^^^^^^^^^^^^^^^

Cross referencing is allowed with the own Quixote magic. Native cross referencing of YAML has not been tested. To cross-reference, one needs to use $label with label being the name of another key in the case dictionary, including abspath. An example is shown below.

A ``!Lambda`` instruction followed by a string can be given as a value of a parameter. These parameters will use data from the SolpsData object and, therefore, can only be used for parsing and ordering after the runs have been loaded.



.. warning::

   ``!Lambda`` uses ``eval``! Therefore, it is potentially unsafe.

   Use only DBs which you trust or use safe=True option when creating
   the Family instance.

   Seting safe=True will set all the ``!Lambda`` entries to None.


.. note::
   
   ``!Lambda`` allows the creation of lambda functions with the following
   limitations:

    * Must be a explicit string, thus wrapped in '' or "". 

    * Must start with ``'lambda x: x.'`` or ``'lambda x: np.'``,
      where np is numpy.

    * Must not contain the symbol ``;``, to avoid command injection.

.. note::

   ``!Lambda`` parameters are not suitable for include or exclude in the
   first pass, only for order. The creation of the DB is done before the
   simulations are loaded and thus ``!Lambda`` is unspecified yet.

   They can be used, however, for filtering after the initial creation.



Examples
^^^^^^^^

As an example, a (slightly modified) sniped from one of my own DBs::

    '34302':
    
      description: "SOLPS-ITER, favourable, error bars"
    
      defaults:
          id: $abspath
          rootpath: /toks/scratch/ipar/solps-iter/runs/aug_ddnu_34302_b2standalone/
          basepath: bestprofs/
          expshot: 34302
          ip: 0.6
          Bt: +2.5
          kaveeva: 1e-2
          dt: 1e-5
          b2standalone: True
          exb:    1.0
          dia:    1.0
          inert:  1.0
          vispar: 1.0
          visper: 1.0
          i-n:    1.0
          status: converged
          leak: -1e-2
          coreflux: 1.00e20
          profs: 1
          teomp_avg: !Lambda 'lambda x: np.average(x.outmp.te[:,x.sep])'
    
      cases:
    
          - path: p01_nesepm=0.60_tecore=175eV_ticore=175eV_coreflux=1.00e20/
            nesepm: 0.60e19
            tecore: 175
            ticore: 175
    
          - path: p01_nesepm=0.60_tecore=200eV_ticore=200eV_coreflux=1.00e20/
            nesepm: 0.60e19
            tecore: 200
            ticore: 200


    
.. warning::

    YAML is a very flimsy language. Some of the main pitfalls are:

    * Not leaving a blank space between : at the end of a label and
      the value, e.g., ticore:300 instead of ticore: 300.

    * Not leaving a black space between - signifying a new element on
      a list and the first label of element, e.g., -path: blablabla instead
      of - path: blablabla. 

    * Not preserving the correct indentation.

    * In general, not knowing or paying attention to the formatting required
      by YAML.

    YAML is very hard to debug because the error messages are not very
    descriptive, easy to interpret or useful.







Basic inputs
------------

Include and exclude
^^^^^^^^^^^^^^^^^^^

**include**, **exclude**: list(dict) containing the values of parameters
to include/exclude.

The structure of both is the same::

    [
        {'param1':[val1, val2, ...], 'param2':[val1, val2,...], ...},
        {'param1':[val1, val2, ...], 'param2':[val1, val2,...], ...},
        ...
    ]

Each individual dictionary will be expanded with all the combinations of
all the values for  all the params.




To illustrate this point:

.. jupyter-execute::

 tmp = [
     {'param1':['p1v1', 'p1v2'], 'param2':['p2v1', 'p2v2']},
     {'param1':['p1v3', 'p1v4'], 'param3':['p3v1', 'p3v2'], 'param4':'p4v1'},
     ]

will result in a list(dict) containing all the criteria to include or
exclude an entry of the full database.

.. jupyter-execute::
   :hide-code:
   :hide-output:

    aux = []
    combinatory = quixote.Family._expand_combinations
    for itm in tmp:
        aux.append(combinatory(itm))
    aux = list(itertools.chain.from_iterable(aux))

.. jupyter-execute::
   :hide-code:

   aux

For each invididual dict, ALL  (Logical AND) the criteria must be met
simulateneously for a database entry to match.

E.g.:

.. jupyter-execute::
   :hide-code:

   aux[4]

will either include or exclude all the entries that contain (but not
necessarily exclusively) ``param1 = p1v3``, ``param3 = p3v1``, and
``param4 = p4v1`` simulatenously.
'Not exclusively' means that entries might have any number of parameters
that won't affect this operation.
**In summary**, criteria work like AND while omitted parameters work like ANY.

The more general criterion will then naturally take precedence over the
more specific ones, as the specific ones are a subset of the general ones.

.. note::

   The DB is first parsed with ``include``, and ``exclude`` is applied to
   the result of that first parsing operation.

.. note::

    In general, it is preferrable to have more general ``include`` and more
    specific ``exclude``. However, this is completely up to the users and
    how it better fits their workflow.





Order
^^^^^


``order``:  ``list(str and/or dict)``, or ``OrderedDict``
(or ``dict`` for Python 3.7+).

The ``order`` argument will set the hiearchy of order of the database
by ordering the columns in a nested pattern from left to right.

There are various different ways to set ``order``:

* ``OrderedDict`` : ``order`` will be set to an exact copy of it.

* ``list(str and/or dict)``: the list will be parsed and either the ``str`` or the ``dict.keys()`` will be used.

    * Wherever ``dict`` or ``OrderedDict`` (recommended) is found, the value defines the way in which the key is to be ordered:

        * ``True``: Ascending order (default of ``pandas``).

        * ``False``: Descending order.

        * ``list(str)``: Exact order according to the given ``list``.

    * ``str`` entries will be set to default ``True``, thus ascending.

.. note::
   ``experiment`` is a special parameter which should be ordered
   in the same way that they are introduced in ``include``.

.. warning::
   
   Using exact order by giving ``{param: list(str)`` is still buggy.
   Sometimes the order works and sometimes it doesn't.
   **Do not use for now.**


For example,::

    order = ['experiment', 'ip', 'necore']

    experiment 1
        ip 11
            necore 111
            necore 112
            ...
        ip 12
            necore 121
            necore 122
            ...

    experiment 2
        ip 21
            necore 211
            necore 212
            ...
        ip 22
            necore 221
            necore 222
            ...
   ... 


.. note::
   From now on, ``order`` can accept ``!Lambda`` parameters, since
   ``_impose_order`` is now applied after ``SolpsData`` objects are loaded.

   However, this requires ``autoload=True``.

.. note::

    In Python 3.7+, ``dict`` ordering is considered a
    feature, and thus it can be trusted.
    However, ``quixote`` will still convert every
    ``order`` input to ``OrderedDict`` internally.




When ``order`` is replaced, ``db``, ``runs``, and ``info`` will be
automatically updated.





.. _uncertainty:

``Uncertainty`` is a given
--------------------------

Death, taxes, and experimental uncertainty. Those are facts of life. 
``Quixote`` will only help you with the last, at least for now.

Two keywords are not reserved and should not be used in general in any DB:
``group`` and ``main``.

The ``Uncertainty`` database will gather all the simulations with the same ``group``
and treat them as variations over the best estimation, i.e., "error bars" of ``main``.

For now, ``main`` is  mandatory keyword, but in the future a group without a ``main`` will use different estimators as representation, such as average, etc.



